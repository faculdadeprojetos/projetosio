import { Fragment, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Dialog, Transition } from "@headlessui/react";
import {
  FolderIcon,
  HomeIcon,
  MenuAlt2Icon,
  UsersIcon,
  XIcon,
  ShoppingCartIcon,
  CloudUploadIcon,
  CurrencyEuroIcon,
  LogoutIcon,
} from "@heroicons/react/outline";
import { SearchIcon } from "@heroicons/react/solid";
import toast, { ErrorIcon, Toaster } from "react-hot-toast";
import ReactLoading from "react-loading";
import Modal from "../components/Modal";


const navigation = [
  { name: "Dashboard", href: "/app", icon: HomeIcon, current: true },
  { name: "Clients", href: "/app/clients", icon: UsersIcon, current: false },
  { name: "Invoices", href: "/app/invoices", icon: FolderIcon, current: false },
  { name: "Products", href: "/app/products", icon: ShoppingCartIcon, current: false },
  { name: "Taxes", href: "/app/taxes", icon: CurrencyEuroIcon, current: false },
];

// Get the current URL path
const path = window.location.pathname;

navigation.forEach((item) => {
  if (item.href === path) {
    item.current = true;
  } else {
    item.current = false;
  }
});

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const Template = (props) => {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  const [modalOpen, setModalOpen] = useState(false);

  const [file, setFile] = useState({
    name: "",
    file: "",
  });

  function toggleModal() {
    setFile("");
    setModalOpen(!modalOpen);
  }

  const inputHandle = (e) => {
    setFile({
      file: e.target.files[0],
      name: e.target.value,
    });
  };

  const notifySucess = () => toast.success("Successfully uploaded Saft-t!\nReload your page");
  const notifyError = () => toast.error("Error trying to upload Saft-t!\nTry again");

  function sendData() {
    if (file !== "") {
      let formData = new FormData();
      formData.append("file", file.file);

      const token = localStorage.getItem("accessToken");

      if (token) {
        fetch("http://localhost:4000/upload-xml", {
          method: "POST",
          body: formData,
          headers: { Authorization: token },
        })
          .then((data) => {
            data.json().then((json) => {
              if (data.ok) {
                notifySucess();
                window.location.reload(false);
              } else {
                notifyError();
              }
            });
          })
          .catch((err) => {
            notifyError();
          });
      } else {
        // IMPLEMENT THIS BLOCK
      }
    }
  }

  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  return (
    <>
      <div>
        <Toaster />
        <Transition.Root show={sidebarOpen} as={Fragment}>
          <Dialog as="div" className="fixed inset-0 z-60 flex mt-10 " onClose={setSidebarOpen}>
            <Transition.Child
              as={Fragment}
              enter="transition-opacity ease-linear duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition-opacity ease-linear duration-300"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-gray-600 bg-opacity-75" />
            </Transition.Child>
            <Transition.Child
              as={Fragment}
              enter="transition ease-in-out duration-300 transform"
              enterFrom="-translate-x-full"
              enterTo="translate-x-0"
              leave="transition ease-in-out duration-300 transform"
              leaveFrom="translate-x-0"
              leaveTo="-translate-x-full"
            >
              <div className="relative max-w-xs w-full bg-white pt-5 pb-4 flex-1 flex flex-col z-40 ">
                <Transition.Child
                  as={Fragment}
                  enter="ease-in-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in-out duration-300"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <div className="absolute top-0 right-0 -mr-12 pt-2">
                    <button
                      type="button"
                      className="ml-1 mt-10 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                      onClick={() => setSidebarOpen(false)}
                    >
                      <span className="sr-only">Close sidebar</span>
                      <XIcon className="h-6 w-6 text-white" aria-hidden="true" />
                    </button>
                  </div>
                </Transition.Child>
                <div className="flex-shrink-0 px-4 flex items-center z-40">
                  <a href="/app">
                    <img className="h-22 w-auto" src={require("./logo.png")} alt="Logo Dashboard" />
                  </a>
                </div>
                <div className="mt-30 flex-1 h-0 overflow-y-auto z-40">
                  <nav className="px-2 space-y-1">
                    {navigation.map((item) => (
                      <a
                        key={item.name}
                        href={item.href}
                        className={classNames(
                          item.current
                            ? "bg-gray-100 text-gray-900"
                            : "text-gray-600 hover:bg-gray-50 hover:text-gray-900",
                          "group rounded-md py-2 px-2 flex items-center text-base font-medium"
                        )}
                      >
                        <item.icon
                          className={classNames(
                            item.current
                              ? "text-gray-500"
                              : "text-gray-400 group-hover:text-gray-500",
                            "mr-4 flex-shrink-0 h-6 w-6"
                          )}
                          aria-hidden="true"
                        />
                        {item.name}
                      </a>
                    ))}
                  </nav>
                </div>
              </div>
            </Transition.Child>
            <div className="flex-shrink-0 w-14">
              {/* Dummy element to force sidebar to shrink to fit close icon */}
            </div>
          </Dialog>
        </Transition.Root>

        {/* Static sidebar for desktop */}
        {/* <div className="hidden md:flex w-64 md:flex-col md:fixed md:inset-y-0 z-0"> */}
        {/* Sidebar component, swap this element with another sidebar if you like */}
        {/* <div className="border-r border-gray-200 pt-5 flex flex-col flex-grow bg-white overflow-y-auto z-40">
            <div className="flex-shrink-0 px-4 flex items-center justify-center">
              <a href="/">
                <img className="h-22 w-auto" src={require("./logo.png")} alt="Logo Dashboard" />
              </a>
            </div>
            <div className="flex-grow mt-5 flex flex-col z-40">
              <nav className="flex-1 px-2 pb-4 space-y-1">
                {navigation.map((item) => (
                  <a
                    key={item.name}
                    href={item.href}
                    className={classNames(
                      item.current
                        ? "bg-gray-100 text-gray-900"
                        : "text-gray-600 hover:bg-gray-50 hover:text-gray-900",
                      "group rounded-md py-3 px-2 flex items-center text-sm font-medium"
                    )}
                  >
                    <item.icon
                      className={classNames(
                        item.current ? "text-gray-500" : "text-gray-400 group-hover:text-gray-500",
                        "mr-3 flex-shrink-0 h-6 w-6"
                      )}
                      aria-hidden="true"
                    />
                    {item.name}
                  </a>
                ))}
              </nav>
            </div>
          </div>
        </div> */}

        <div className="z-40">
          <div className="flex flex-col z-40">
            <div className="sticky top-0 z-10 flex-shrink-0 h-16 bg-white border-b border-gray-200 flex">
              <button
                type="button"
                className="border-r border-gray-200 px-4 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500 ml-10"
                onClick={() => setSidebarOpen(true)}
              >
                <span className="sr-only">Open sidebar</span>
                <MenuAlt2Icon className="h-6 w-6" aria-hidden="true" />
              </button>
              <div className="flex justify-between px-4 w-full">

                <div className="flex justify-start">
                  <button
                    className="flex space-x-3 items-center p-1 rounded-full text-gray-400 hover:text-gray-500"
                    onClick={() => {
                      navigate("/app");
                    }}
                  >
                    <HomeIcon className="h-6 w-6" aria-hidden="true" />
                    <span className="">Home</span>
                  </button>
                  <button
                    className="flex space-x-3 items-center p-1 rounded-full text-gray-400 hover:text-gray-500 ml-10"
                    onClick={() => {
                      toggleModal();
                    }}
                  >
                    <CloudUploadIcon className="h-6 w-6" aria-hidden="true" />
                    <span className="">Upload Saft-t</span>
                  </button>
                </div>

                <div className="flex justify-start">
                <button
                  className="flex space-x-3 items-center p-1 rounded-full text-gray-400 hover:text-gray-500"
                  onClick={() => {
                    setIsLoading(true);
                    setIsLoading(true)
                    localStorage.removeItem('cachedStats');

                    const token = localStorage.getItem("accessToken");

                    if (token) {
                      fetch("http://localhost:4000/delete", { headers: { Authorization: token } })
                        .then((data) => {
                          data.json().then((json) => {
                            toast.success("Information deleted!");
                          });
                        })
                        .catch((err) => {
                          toast.error("Error trying to delete information");
                          //TODO: TOAST ERROR
                        })
                        .finally(() => {
                          setIsLoading(false);
                        });
                    } else {
                      // HANDLE THIS BLOCK
                    }
                  }}
                >
                  <XIcon className="h-6 w-6 text-red-700" aria-hidden="true" />
                  <span className="text-red-700">Clear information</span>
                </button>
              </div>
            </div>
            <button
                    className="flex space-x-3 items-center p-1 rounded-full text-gray-400 hover:text-gray-500 mr-10"
                    onClick={() => {
                      localStorage.removeItem("accessToken");
                      navigate("/");
                    }}
                  >
                    <LogoutIcon className="h-6 w-6" aria-hidden="true" />
                    <span className="">Logout</span>
                  </button>
            </div>

            <main className="flex-1 h-full mx-40">
              <div className="py-1">
                {isLoading && (
                  <div className="flex flex-wrap items-center justify-center pt-24 ">
                    <ReactLoading type={"bubbles"} height={100} width={120} color={"#5b2bab"} />
                    <span className="mb-4 w-full self-start text-center text-2xl text-lg font-extrabold tracking-tight dark:text-white xl:text-2xl">
                      Loading
                    </span>
                  </div>
                )}

                {!isLoading && <div className="px-4 sm:px-6 md:px-0">{props.children}</div>}
              </div>
            </main>
          </div>
        </div>
      </div >
      {modalOpen && (
        <Modal
          onClick={() => {
            sendData();
            toggleModal();
          }}
          toggleModal={toggleModal}
          urlFile={file}
          inputHandle={inputHandle}
          setFile={setFile}
        />
      )
      }
    </>
  );
};

export default Template;

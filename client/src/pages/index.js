import Dashboard from "./Dashboard";
import Invoices from "./Invoices";
import Clients from "./Clients";
import Products from "./Products";
import Taxes from "./Taxes";
import Auth from "./Auth";

export { Dashboard, Invoices, Clients, Products, Taxes, Auth };

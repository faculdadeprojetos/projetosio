import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import ReactLoading from "react-loading";
import toast, { Toaster } from "react-hot-toast";

const Register = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [errors, setErrors] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();

  const handleRegister = (event) => {
    event.preventDefault();
    const errObject = {};

    if (username.length < 3 || username.length > 10) {
      errObject.username = "username must have 3 to 10 characters";
    }
    if (password.length < 4) {
      errObject.password = "password is to small";
    }
    if (password !== confirmPassword) {
      errObject.cpassword = "passwords do not match";
    }

    if (errObject.cpassword || errObject.password || errObject.username) {
      setErrors(errObject);
    } else {
      setErrors({});
      const data = {
        username: username,
        email: email,
        password: password,
        confirmPassword: confirmPassword,
      };

      setIsLoading(true);
      fetch("http://localhost:4000/auth/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          setIsLoading(false);
          if (data.token) {
            localStorage.setItem("accessToken", data.token);
            navigate("/app");
          } else if (data.message) {
            toast.error(data.message);
          }
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  };

  return (
    <div>
      <Toaster />
      <div className="min-h-screen flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <img className="h-22 w-auto" src={require("../layout/logo.png")} alt="Logo Dashboard" />
          </div>
          <form className="mt-8 space-y-6" onSubmit={handleRegister}>
            <div className="rounded-md shadow-sm space-y-4">
              <div className="flex flex-col space-y-2">
                {errors.username !== null ? (
                  <span className="text-red-600 font-semibold text-sm">{errors.username}</span>
                ) : null}
                <div>
                  <label htmlFor="username" className="sr-only">
                    Username
                  </label>
                  <input
                    id="username"
                    name="username"
                    type="text"
                    required
                    className="appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-cyan-500 focus:border-cyan-500 focus:z-10 sm:text-sm"
                    placeholder="Username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </div>
              </div>

              <div>
                <label htmlFor="email" className="sr-only">
                  Email
                </label>
                <input
                  id="email"
                  name="email"
                  type="email"
                  required
                  className="appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-cyan-500 focus:border-cyan-500 focus:z-10 sm:text-sm"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div className="flex flex-col space-y-2">
                {errors.password !== null ? (
                  <span className="text-red-600 font-semibold text-sm">{errors.password}</span>
                ) : null}
                <div>
                  <label htmlFor="password" className="sr-only">
                    Password
                  </label>
                  <input
                    id="password"
                    name="password"
                    type="password"
                    required
                    className="appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-cyan-500 focus:border-cyan-500 focus:z-10 sm:text-sm"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
              </div>
              <div className="flex flex-col space-y-2">
                {errors.cpassword !== null ? (
                  <span className="text-red-600 font-semibold text-sm">{errors.cpassword}</span>
                ) : null}
                <div>
                  <label htmlFor="confirmPassword" className="sr-only">
                    Confirm Password
                  </label>
                  <input
                    id="confirmPassword"
                    name="confirmPassword"
                    type="password"
                    required
                    className="appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-cyan-500 focus:border-cyan-500 focus:z-10 sm:text-sm"
                    placeholder="Confirm Password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                  />
                </div>
              </div>
            </div>
            <div>
              <button
                type="submit"
                className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-cyan-600 hover:bg-cyan-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-500"
              >
                Register
              </button>
            </div>
          </form>
          {isLoading ? (
            <div className="flex flex-wrap items-center justify-center">
              <ReactLoading type={"cylon"} height={4} width={50} color={"#0891b2"} />
            </div>
          ) : (
            <div className="hidden">
              <ReactLoading type={"cylon"} height={4} width={50} color={"#0891b2"} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Register;

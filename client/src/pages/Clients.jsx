import React, { Fragment, useEffect, useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import { Bar, Doughnut } from "react-chartjs-2";
import Chart from "chart.js/auto";
import { useNavigate } from "react-router-dom";
import { Line, Pie } from "react-chartjs-2";
import { Dialog, Menu, Transition } from "@headlessui/react";
import {
  CashIcon,
  CogIcon,
  CreditCardIcon,
  CurrencyEuroIcon,
  DocumentDownloadIcon,
  DocumentRemoveIcon,
  EyeIcon,
  PencilAltIcon,
  XIcon,
} from "@heroicons/react/outline";
import ReactLoading from "react-loading";
import {
  SearchCircleIcon,
  ArrowNarrowDownIcon,
  ChevronDownIcon,
  ArrowNarrowUpIcon,
} from "@heroicons/react/outline";

const Clients = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [clients, setClients] = useState([]);
  const [individualClient, setIndividualClient] = useState("");
  const [dashboardValues, setDashboardValues] = useState({});
  const [searchBarClient, setSearchBarClient] = useState("");
  const navigate = useNavigate();
  const [sortType, setSortType] = useState("Name descending");
  const [isModalOpen, setIsModelOpen] = useState(false);
  const sortOptions = [
    "Gross descending",
    "Gross ascending",
    "Taxes descending",
    "Taxes ascending",
    "Name ascending",
    "Name descending",
  ];

  useEffect(() => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      fetch("http://localhost:4000/clients", { headers: { Authorization: token } })
        .then((data) => {
          data.json().then((json) => {
            if (data.ok) {
              setIsLoading(false);
              setClients(json.clients);
            } else {
              setIsLoading(false);
              //TODO: TOAST ERROR
            }
          });
        })
        .catch((err) => {
          console.log(err);
          toast.error("Session expired")
        });
    } else {
      // IMPLEMENT THIS BLOCK
      toast.error("Session expired")
      navigate("/")
    }
  }, []);

  // DASHBOARD
  const generateLineChartData = (datasets) => {
    var result = {};

    result.labels = Object.keys(dashboardValues).reverse();
    var resultDatasets = [];

    datasets.forEach((dataset) => {
      resultDatasets.push({
        label: dataset.label,
        fill: false,
        lineTension: 0.1,
        backgroundColor: dataset.backgroundColor,
        borderColor: dataset.borderColor,
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: dataset.pointBorderColor,
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: dataset.pointHoverBackgroundColor,
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: dataset.values,
      });
    });
    result.datasets = resultDatasets;
    return result;
  };

  const [chartDataValues, setChartDataValues] = useState({
    labels: [],
    datasets: [{}],
  });

  const options = {
    responsive: true,
    scales: {
      x: {
        border: {
          color: "#cacaca",
        },
        grid: {
          display: false,
        },
      },
      y: {
        border: {
          color: "#cacaca",
        },
        grid: {
          display: false,
        },
      },
    },
  };
  // END DASHBOARD

  //auxiliary function to sort array
  function getSortResult(a, b) {
    if (sortType === "Gross ascending") {
      return a.total_gross - b.total_gross;
    } else if (sortType === "Gross descending") {
      return b.total_gross - a.total_gross;
    } else if (sortType === "Taxes ascending") {
      return a.total_taxes - b.total_taxes;
    } else if (sortType === "Taxes descending") {
      return b.total_taxes - a.total_taxes;
    } else if (sortType === "Name ascending") {
      if (a.client_name < b.client_name) {
        return -1;
      }
      if (a.client_name > b.client_name) {
        return 1;
      }
      return 0;
    } else if (sortType === "Name descending") {
      if (a.client_name < b.client_name) {
        return 1;
      }
      if (a.client_name > b.client_name) {
        return -1;
      }
      return 0;
    }
  }

  //sort purposes
  useEffect(() => {
    let tmp = JSON.parse(JSON.stringify(clients));
    tmp.sort(function (a, b) {
      return getSortResult(a, b);
    });
    setClients(tmp);
  }, [sortType]);

  //toggles the model
  function toggleModel() {
    setIsModelOpen(!isModalOpen);
  }

  //get the individual information about the customer
  function getCustomerInfo(id) {
    let user = {};
    console.log(id);
    for (let i = 0; i < clients.length; i++) {
      if (clients[i].client_id === id) {
        console.log("innnn");
        user = clients[i];
      }
    }
    console.log(user);

    const token = localStorage.getItem("accessToken");

    if (token) {
      //get extra information
      fetch(`http://localhost:4000/clients/customerInfo/${id}`, {
        headers: { Authorization: token },
      })
        .then((data) => {
          data.json().then((json) => {
            if (data.ok) {
              setIsLoading(false);
              //get personal information
              let newInfo = {};
              newInfo.basicInfo = user;
              newInfo.billingAddresses = json.addressInfo;
              newInfo.invoiceInfo = json.invoiceInfo;
              newInfo.productsInfo = json.productsInfo;
              newInfo.productFamiliesInfo = json.productFamiliesInfo;
              console.log(newInfo);
              getDashboardInfo(json.invoiceInfo, json.productFamiliesInfo);
              setIndividualClient(newInfo);
            } else {
              setIsLoading(false);
              //TODO: TOAST ERROR
            }
          });
        })
        .catch((err) => {
          console.log(err);
          alert("Error getting data");
          //TODO: TOAST ERROR
        });
    } else {
      // IMPLEMENT THIS BLOCK
      toast.error("Session expired")
      navigate("/")
      // IMPLEMENT THIS BLOCK
    }
  }

  //get the information fot the dashboard
  function getDashboardInfo(invoiceInfo, productFamInfo) {
    let values = {};

    invoiceInfo.forEach((invoice) => {
      let date = new Date(invoice.invoice_date).toLocaleDateString("en-GB").split("/");
      let monthYear = date[1] + "-" + date[2];
      if (values.monthYear) {
        values[monthYear] = values[monthYear] + invoice.gross_total;
      } else {
        values[monthYear] = invoice.gross_total;
      }
    });
    console.log('values',values);
    setDashboardValues(values);

    let labels = [];
    let data = [];

    for (let i = 0; i < productFamInfo.length; i++) {
      labels.push(productFamInfo[i].product_group);
      data.push(productFamInfo[i].total_group);
    }

    let dataset = [];
    dataset.push({
      label: "Number of products bought",
      data: data,
      backgroundColor: ["rgba(53, 162, 235, 0.5)"],
      borderColor: ["rgba(14, 29, 210, 0.8)"],
      borderWidth: 1,
    });

    setChartDataValues({ labels, datasets: dataset });
  }

  //truncate a given string
  function truncate(str) {
    return str.length > 45 ? str.substring(0, 45) + "..." : str;
  }
  return (
    <div>
      <div className="px-4 sm:px-6 md:px-0 mb-4 mt-2">
        <h1 className="text-2xl font-semibold text-gray-900 mb-10">Clients</h1>
      </div>

      {/* GRAPHICS  */}
      <div></div>

      {/* FILTER SECTION */}
      <div className="w-full flex justify-end">
        <div className="mb-3 xl:w-64">
          <div className="relative mb-4 flex w-full flex-wrap items-stretch">
            <input
              type="search"
              value={searchBarClient}
              onChange={(e) => {
                setSearchBarClient(e.target.value);
              }}
              className="relative m-0 block w-[1%] min-w-0 flex-auto rounded border border-solid border-gray-500 bg-transparent bg-clip-padding px-3 py-1.5 text-base font-normal text-gray-700 outline-none transition duration-300 ease-in-out focus:border-gray-600 focus:text-gray-00"
              placeholder="Client name"
              aria-label="Client name"
              aria-describedby="button-addon2"
            />
          </div>
        </div>

        <div className="ml-10">
          <Menu as="div" className="relative justify-end text-left">
            <div>
              <Menu.Button className="group inline-flex justify-center text-lg font-medium text-gray-500 hover:text-gray-900">
                Sort
                <ChevronDownIcon
                  className="-mr-1 ml-1 h-8 w-8 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                  aria-hidden="true"
                />
              </Menu.Button>
            </div>

            <Transition
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="absolute right-0 z-10 mt-2 w-40 origin-top-right rounded-md bg-white shadow-2xl ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="py-1">
                  {sortOptions.map((option, key) => (
                    <Menu.Item key={key}>
                      <p
                        onClick={() => {
                          setSortType(option);
                        }}
                        className={`${option === sortType ? "font-medium text-gray-900" : "text-gray-500"
                          }
                            block cursor-pointer px-4 py-2 text-sm`}
                      >
                        {option}
                      </p>
                    </Menu.Item>
                  ))}
                </div>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
      </div>

      {isLoading && (
        <div className="flex flex-wrap items-center justify-center pt-24 ">
          <ReactLoading type={"bubbles"} height={100} width={120} color={"#5b2bab"} />
          <span className="mb-4 w-full self-start text-center text-2xl text-lg font-extrabold tracking-tight dark:text-white xl:text-2xl">
            Loading
          </span>
        </div>
      )}
      {!isLoading && clients.length > 0 && (
        <div className="flex flex-col">
          <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
              <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table className="min-w-full divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        ID
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        <div className="w-full flex">
                          <span className="my-auto">NAME</span>
                          {sortType === "Name ascending" && (
                            <ArrowNarrowUpIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                          {sortType === "Name descending" && (
                            <ArrowNarrowDownIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                        </div>
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        <div className="w-full flex">
                          <span className="my-auto">TOTAL GROSS</span>
                          {sortType === "Gross ascending" && (
                            <ArrowNarrowUpIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                          {sortType === "Gross descending" && (
                            <ArrowNarrowDownIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                        </div>
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        <div className="w-full flex">
                          <span className="my-auto">TOTAL TAXES</span>
                          {sortType === "Taxes ascending" && (
                            <ArrowNarrowUpIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                          {sortType === "Taxes descending" && (
                            <ArrowNarrowDownIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                        </div>
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      ></th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {clients.map((client) => (
                      <React.Fragment key={client.client_id}>
                        {client.client_name
                          .toLowerCase()
                          .includes(searchBarClient.toLowerCase()) && (
                            <tr key={client.client_id}>
                              <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {client.client_id}
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {client.client_name}
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {client.total_gross} €
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {client.total_taxes} €
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                <button
                                  onClick={() => {
                                    toggleModel();
                                    getCustomerInfo(client.client_id);
                                  }}
                                >
                                  <SearchCircleIcon className="h-6 w-6" />
                                </button>
                              </td>
                            </tr>
                          )}
                      </React.Fragment>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          {/* MODAL TO SHOW THE CUSTOMER PERSONAL INFORMATION */}
          {isModalOpen && individualClient && (
            <Transition.Root show={true} as={Fragment}>
              <Dialog as="div" className="fixed inset-0 z-10 overflow-y-auto" onClose={toggleModel}>
                <div className="flex min-h-screen items-end justify-center px-4 pt-4 pb-20 text-center sm:block sm:p-0">
                  <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                  >
                    <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                  </Transition.Child>

                  {/* This element is to trick the browser into centering the modal contents. */}
                  <span
                    className="hidden sm:inline-block sm:h-screen sm:align-middle"
                    aria-hidden="true"
                  >
                    &#8203;
                  </span>
                  <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    enterTo="opacity-100 translate-y-0 sm:scale-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                    leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                  >
                    <div className="inline-block transform overflow-hidden rounded-lg bg-white p-8 text-left align-bottom shadow-xl transition-all sm:my-8 sm:align-middle">
                      <div className="absolute top-0 right-0 hidden pt-4 pr-4 sm:block">
                        <button
                          type="button"
                          className="rounded-md bg-white text-gray-900 focus:ring-offset-2 hover:text-gray-500"
                          onClick={toggleModel}
                        >
                          <span className="sr-only">Close</span>
                          <XIcon className="h-6 w-6" aria-hidden="true" />
                        </button>
                      </div>
                      <div className="sm:flex sm:items-start">
                        <div className="mx-auto flex h-12 w-12 flex-shrink-0 items-center justify-center rounded-full bg-gray-300 sm:mx-0 sm:h-10 sm:w-10">
                          <DocumentDownloadIcon
                            className="h-6 w-6 text-gray-900"
                            aria-hidden="true"
                          />
                        </div>
                        <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left mr-10 mb-5">
                          <Dialog.Title
                            as="h3"
                            className="text-xl font-bold leading-6 text-gray-900 text-center"
                          >
                            Name: {individualClient.basicInfo.client_name}
                          </Dialog.Title>

                          {/* FIRST ROW */}
                          <div className="mt-8 gap-6 grid grid-cols-2 gap-10">
                            {/* INVOICE INFORMATION */}
                            <div className="text-sm text-gray-500 dark:text-gray-300 span-col-2">
                              <div className="pt-10">
                                <p className="text-lg  font-bold leading-6 text-gray-900 mb-4">
                                  Invoice totals
                                </p>
                                <dl className="grid grid-cols-3 gap-5">
                                  <div className="relative overflow-hidden rounded-lg bg-gray-200 px-4 pt-2 shadow  sm:px-6 sm:pt-6">
                                    <dt>
                                      <div className="absolute rounded-md bg-indigo-500 p-3">
                                        <CashIcon
                                          className="h-4 w-4 text-white"
                                          aria-hidden="true"
                                        />
                                      </div>
                                      <p className="ml-16 truncate text-lg font-medium text-gray-500 ">
                                        Total gross
                                      </p>
                                    </dt>
                                    <dd className="ml-16 flex items-baseline pb-6 sm:pb-7">
                                      <p className="text-lg font-semibold text-gray-700 ">
                                        {individualClient.basicInfo.total_gross} €
                                      </p>
                                    </dd>
                                  </div>

                                  <div className="relative overflow-hidden rounded-lg bg-gray-200 px-4 pt-5 shadow  sm:px-6 sm:pt-6">
                                    <dt>
                                      <div className="absolute rounded-md bg-indigo-500 p-3">
                                        <CurrencyEuroIcon
                                          className="h-4 w-4 text-white"
                                          aria-hidden="true"
                                        />
                                      </div>
                                      <p className="ml-16 truncate text-lg font-medium text-gray-500 ">
                                        Total net
                                      </p>
                                    </dt>
                                    <dd className="ml-16 flex items-baseline pb-6 sm:pb-7">
                                      <p className="text-lg font-semibold text-gray-700 ">
                                        {individualClient.basicInfo.total_net} €
                                      </p>
                                    </dd>
                                  </div>

                                  <div className="relative overflow-hidden rounded-lg bg-gray-200 px-4 pt-5 shadow  sm:px-6 sm:pt-6">
                                    <dt>
                                      <div className="absolute rounded-md bg-indigo-500 p-3">
                                        <CreditCardIcon
                                          className="h-4 w-4 text-white"
                                          aria-hidden="true"
                                        />
                                      </div>
                                      <p className="ml-16 truncate text-lg font-medium text-gray-500 ">
                                        Total taxes
                                      </p>
                                    </dt>
                                    <dd className="ml-16 flex items-baseline pb-6 sm:pb-7">
                                      <p className="text-lg font-semibold text-gray-700 ">
                                        {individualClient.basicInfo.total_taxes} €
                                      </p>
                                    </dd>
                                  </div>
                                </dl>
                              </div>
                            </div>

                            {/* BILLING INFO */}
                            <div className="span-col-1 mt-8">
                              <div className="text-sm text-gray-500 dark:text-gray-300">
                                <div className="col-span-4 md:col-span-3">
                                  <p className="text-lg  font-bold leading-6 text-gray-900 mb-6">
                                    Billing information
                                  </p>
                                  <table className="min-w-full divide-y divide-gray-200 border border-gray-500">
                                    <thead className="bg-gray-300 border border-gray-500">
                                      <tr>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          City
                                        </th>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Postal code
                                        </th>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Country
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody className="bg-gray-100 divide-y divide-gray-400">
                                      {individualClient.billingAddresses.map((address) => (
                                        <tr key={address.city}>
                                          <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                            {address.city}
                                          </td>
                                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            {address.postal_code}
                                          </td>
                                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            {address.country}
                                          </td>
                                        </tr>
                                      ))}
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>

                          {/* SECOND ROW */}
                          <div className="mt-8 gap-6 grid grid-cols-2 gap-10">
                            {/* INVOICE */}
                            <div className="span-col-1 mt-8">
                              <div className="text-sm text-gray-500 dark:text-gray-300">
                                <div className="col-span-4 md:col-span-3">
                                  <p className="text-lg  font-bold leading-6 text-gray-900 mb-4">
                                    Latest invoices
                                    <p className="text-sm font-bold leading-4 text-gray-500 mb-4 mt-2">
                                      out of an total of {individualClient.invoiceInfo.length}{" "}
                                      invoices
                                    </p>
                                  </p>
                                  <table className="min-w-full divide-y divide-gray-200 border border-gray-500">
                                    <thead className="bg-gray-300 border border-gray-500">
                                      <tr>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Date
                                        </th>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Invoice number
                                        </th>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Invoice type
                                        </th>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Total of items
                                        </th>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Gross total
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody className="bg-gray-100 divide-y divide-gray-400">
                                      {(() => {
                                        const rows = [];
                                        for (
                                          let i = 0;
                                          i < individualClient.invoiceInfo.length;
                                          i++
                                        ) {
                                          if (i >= 5) {
                                            break;
                                          }
                                          const invoice = individualClient.invoiceInfo[i];
                                          rows.push(
                                            <tr key={invoice.invoice_no}>
                                              <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                                {new Date(invoice.invoice_date).toLocaleDateString(
                                                  "en-GB"
                                                )}
                                              </td>
                                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                {invoice.invoice_no}
                                              </td>
                                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                {invoice.invoice_type}
                                              </td>
                                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                {invoice.total_items}
                                              </td>
                                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                {invoice.gross_total} €
                                              </td>
                                            </tr>
                                          );
                                        }
                                        return rows;
                                      })()}
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>

                            {/* INVOICE GRAPH */}
                            <div className="span-col-1 mt-9">
                              <div className="text-sm text-gray-500 dark:text-gray-300">
                                <div className="col-span-4 md:col-span-3">
                                  <p className="text-lg  font-bold leading-6 text-gray-900 mb-4">
                                    Graph of invoices
                                  </p>
                                  <Line
                                    options={options}
                                    responsive="true"
                                    data={generateLineChartData([
                                      {
                                        label: "Invoices gross totals",
                                        values: Object.values(dashboardValues).reverse(),
                                        pointBorderColor: "#4495e2",
                                        pointHoverBackgroundColor: "#4495e2",
                                        borderColor: "#4495e2",
                                        backgroundColor: "#4495e2",
                                      },
                                    ])}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                          {/* THIRD ROW */}
                          <div className="mt-8 gap-6 grid grid-cols-2 gap-10">
                            {/* INVOICE */}
                            <div className="span-col-1 mt-8">
                              <div className="text-sm text-gray-500 dark:text-gray-300">
                                <div className="col-span-4 md:col-span-3">
                                  <p className="text-lg  font-bold leading-6 text-gray-900 mb-4">
                                    Top products
                                    <p className="text-sm font-bold leading-4 text-gray-500 mb-4 mt-2">
                                      out of an total of {individualClient.productsInfo.length}{" "}
                                      different products bought
                                    </p>
                                  </p>
                                  <table className="min-w-full divide-y divide-gray-200 border border-gray-500">
                                    <thead className="bg-gray-300 border border-gray-500">
                                      <tr>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Product code
                                        </th>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Product description
                                        </th>
                                        <th
                                          scope="col"
                                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >
                                          Total of buys
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody className="bg-gray-100 divide-y divide-gray-400">
                                      {(() => {
                                        const rows = [];
                                        for (
                                          let j = 0;
                                          j < individualClient.productsInfo.length;
                                          j++
                                        ) {
                                          if (j >= 5) {
                                            break;
                                          }
                                          const product = individualClient.productsInfo[j];
                                          rows.push(
                                            <tr key={product.product_code}>
                                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900 font-bold">
                                                {product.product_code}{" "}
                                              </td>
                                              <td
                                                className="px-6 py-4 whitespace-nowrap text-sm text-gray-500"
                                                title={`${product.product_description}`}
                                              >
                                                {truncate(product.product_description)}
                                              </td>
                                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                {product.total_product}
                                              </td>
                                            </tr>
                                          );
                                        }
                                        return rows;
                                      })()}
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>

                            {/* INVOICE GRAPH */}
                            <div className="span-col-1 mt-8">
                              <div className="text-sm text-gray-500 dark:text-gray-300">
                                <div className="col-span-4 md:col-span-3">
                                  <p className="text-lg  font-bold leading-6 text-gray-900 mb-4">
                                    Product families
                                  </p>
                                  <Bar
                                    data={chartDataValues}
                                    responsive="true"
                                    options={{
                                      scales: {
                                        y: {
                                          suggestedMin: 0,
                                          suggestedMax:
                                            individualClient.productFamiliesInfo[0].total_group +
                                            individualClient.productFamiliesInfo[0].total_group / 3,
                                          ticks: {
                                            beginAtZero: true,
                                            precision: 0,
                                            stepSize: 1,
                                          },
                                        },
                                      },
                                      plugins: {
                                        title: {
                                          display: true,
                                          align: "center",
                                          text: "Family products",
                                        },
                                      },
                                    }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Transition.Child>
                </div>
              </Dialog>
            </Transition.Root>
          )}
        </div>
      )}
      {!isLoading && clients.length === 0 && (
        <h2 className="text-center text-lg font-extrabold m-5">No clients found</h2>
      )}
    </div>
  );
};

export default Clients;

import React, { Fragment, useEffect, useState } from "react";
import { Line } from "react-chartjs-2";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { Menu, Transition } from "@headlessui/react";
import {
  CashIcon,
  CurrencyEuroIcon,
  XIcon,
} from "@heroicons/react/outline";
import ReactLoading from "react-loading";
import {
  SearchCircleIcon,
  ArrowNarrowDownIcon,
  ChevronDownIcon,
  ArrowNarrowUpIcon,
} from "@heroicons/react/outline";

const Products = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const [individualProduct, setindividualProduct] = useState("");
  const [dashboardValues, setDashboardValues] = useState({});
  const [searchBarProduct, setSearchBarProduct] = useState("");
  const [sortType, setSortType] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const navigate = useNavigate();

  //PAGINATION
  const [numOfItems, setNumOfItems] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [indexIntervals, setIndexIntervals] = useState();
  const [maxPage, setMaxPage] = useState();

  const sortOptions = [
    "Type descending",
    "Type ascending",
    "Code descending",
    "Code ascending",
    "Description descending",
    "Description ascending",
    "Number Code descending",
    "Number Code ascending",
    "Group descending",
    "Group ascending",
  ];

  useEffect(() => {
    setMaxPage(Math.floor(products.length / numOfItems));
    setCurrentPage(1);
  }, [products.length, numOfItems]);

  useEffect(() => {
    const end = currentPage * numOfItems - 1;
    const start = end - numOfItems + 1;
    setIndexIntervals([start, end]);
  }, [currentPage, numOfItems]);

  // DASHBOARD
  const generateLineChartData = (datasets) => {
    var result = {};

    result.labels = Object.keys(dashboardValues);
    var resultDatasets = [];

    datasets.forEach((dataset) => {
      resultDatasets.push({
        label: dataset.label,
        fill: false,
        lineTension: 0.1,
        backgroundColor: dataset.backgroundColor,
        borderColor: dataset.borderColor,
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: dataset.pointBorderColor,
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: dataset.pointHoverBackgroundColor,
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: dataset.values,
      });
    });
    result.datasets = resultDatasets;
    return result;
  };

  const options = {
    responsive: true,
    scales: {
      x: {
        border: {
          color: "#cacaca",
        },
        grid: {
          display: false,
        },
      },
      y: {
        border: {
          color: "#cacaca",
        },
        grid: {
          display: false,
        },
      },
    },
  };
  // END DASHBOARD

  //function to sort array
  function getSortResult(a, b) {
    if (sortType === "Type ascending") {
      if (a.product_type < b.product_type) {
        return -1;
      }
      if (a.product_type > b.product_type) {
        return 1;
      }
      return 0;
    } else if (sortType === "Type descending") {
      if (a.product_type < b.product_type) {
        return 1;
      }
      if (a.product_type > b.product_type) {
        return -1;
      }
      return 0;
    }
    else if (sortType === "Code ascending") {
      return a.product_code - b.product_code;
    } else if (sortType === "Code descending") {
      return b.product_code - a.product_code;
    } else if (sortType === "Description ascending") {
      if (a.product_description < b.product_description) {
        return -1;
      }
      if (a.product_description > b.product_description) {
        return 1;
      }
      return 0;
    } else if (sortType === "Description descending") {
      if (a.product_description < b.product_description) {
        return 1;
      }
      if (a.product_description > b.product_description) {
        return -1;
      }
      return 0;
    } else if (sortType === "Number Code ascending") {
      return a.product_number_code - b.product_number_code;
    } else if (sortType === "Number Code descending") {
      return b.product_number_code - a.product_number_code;
    } else if (sortType === "Group ascending") {
      if (a.product_group < b.product_group) {
        return -1;
      }
      if (a.product_group > b.product_group) {
        return 1;
      }
      return 0;
    } else if (sortType === "Group descending") {
      if (a.product_group < b.product_group) {
        return 1;
      }
      if (a.product_group > b.product_group) {
        return -1;
      }
      return 0;
    }
  }

  useEffect(() => {
    let tmp = JSON.parse(JSON.stringify(products));
    tmp.sort(function (a, b) {
      return getSortResult(a, b);
    });
    setProducts(tmp);
  }, [sortType]);


  //toggles the modal
  function toggleModal() {
    setIsModalOpen(!isModalOpen);
  }

  //get the individual information about the product
  function getProductInfo(id) {
    let product = {};
    for (let i = 0; i < products.length; i++) {
      if (products[i].product_id === id) {
        product = products[i];
      }
    }

    const token = localStorage.getItem("accessToken");

    if (token) {
      //get extra information
      fetch(`http://localhost:4000/products/productInfo/${id}`, {
        headers: { Authorization: token },
      })
        .then((data) => {
          data.json().then((json) => {
            if (data.ok) {
              setIsLoading(false);
              //get information
              let newInfo = {};
              newInfo.basicInfo = product;
              console.log("JSONNNNNNNNNNNNNNNN: ");
              console.log(json);
              newInfo.total_in_sales = json.total_in_sales[0].total_in_sales;
              newInfo.total_sales = json.total_sales[0].total_sales;
              newInfo.best_local = json.best_local[0];
              newInfo.last_product_sales = json.product_sales;
              console.log("NEW INFOOOOOOOOOOOOOO: ");
              console.log(newInfo);
              getDashboardInfo(json.product_sales);
              setindividualProduct(newInfo);
            } else {
              setIsLoading(false);
              toast.error("Error getting data");
              return false;
            }
          });
        })
        .catch((err) => {
          console.log(err);
          toast.error("An unexpected error occured");
        });
    } else {
      toast.error("Session expired")
      navigate("/")
    }
  }

  //get the information fot the dashboard
  function getDashboardInfo(invoiceInfo) {
    let values = {};

    invoiceInfo.forEach((invoice) => {
      let date = new Date(invoice.invoice_date).toLocaleDateString("en-GB").split("/");
      let monthYear = date[1] + "-" + date[2];
      if (values.monthYear) {
        values[monthYear] = values[monthYear] + invoice.gross_total;
      } else {
        values[monthYear] = invoice.gross_total;
      }
    });
    setDashboardValues(values);

    let labels = [];
    let data = [];

    let dataset = [];
    dataset.push({
      label: "Gross total",
      data: data,
      backgroundColor: ["rgba(53, 162, 235, 0.5)"],
      borderColor: ["rgba(14, 29, 210, 0.8)"],
      borderWidth: 1,
    });
  }

  useEffect(() => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      fetch("http://localhost:4000/products", { headers: { Authorization: token } })
        .then((data) => {
          data.json().then((json) => {
            if (data.ok) {
              setIsLoading(false);
              setProducts(json.products);
            } else {
              setIsLoading(false);
              toast.error("Error getting data");
            }
          });
        })
        .catch((err) => {
          console.log(err);
          toast.error("An unexpected error occured");
        });
    } else {
      toast.error("Session expired")
      navigate("/")
    }
  }, []);

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  return (
    <div>
      <Toaster />
      <div className="px-4 sm:px-6 md:px-0 mb-4 mt-2">
        <h1 className="text-2xl font-semibold text-gray-900">Products</h1>
      </div>
      <div className="w-full flex justify-end">
        <div className="mb-3 xl:w-64">
          <div className="relative mb-4 flex w-full flex-wrap items-stretch">
            <input
              type="search"
              value={searchBarProduct}
              onChange={(e) => {
                setSearchBarProduct(e.target.value);
              }}
              className="relative m-0 block w-[1%] min-w-0 flex-auto rounded border border-solid border-gray-500 bg-transparent bg-clip-padding px-3 py-1.5 text-base font-normal text-gray-700 outline-none transition duration-300 ease-in-out focus:border-gray-600 focus:text-gray-00"
              placeholder="Product description"
              aria-label="Product description"
              aria-describedby="button-addon2"
            />
          </div>
        </div>
        <div className="ml-10">
          <Menu as="div" className="relative justify-end text-left">
            <div>
              <Menu.Button className="group inline-flex justify-center text-lg font-medium text-gray-500 hover:text-gray-900">
                Sort
                <ChevronDownIcon
                  className="-mr-1 ml-1 h-8 w-8 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                  aria-hidden="true"
                />
              </Menu.Button>
            </div>

            <Transition
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="absolute right-0 z-10 mt-2 w-40 origin-top-right rounded-md bg-white shadow-2xl ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="py-1">
                  {sortOptions.map((option, key) => (
                    <Menu.Item key={key}>
                      <p
                        onClick={() => {
                          setSortType(option);
                        }}
                        className={`${option === sortType ? "font-medium text-gray-900" : "text-gray-500"
                          }
                            block cursor-pointer px-4 py-2 text-sm`}
                      >
                        {option}
                      </p>
                    </Menu.Item>
                  ))}
                </div>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
      </div>
      {isLoading && (
        <div className="flex flex-wrap items-center justify-center pt-24 ">
          <ReactLoading type={"bubbles"} height={100} width={120} color={"#5b2bab"} />
          <span className="mb-4 w-full self-start text-center text-2xl font-extrabold tracking-tight dark:text-white xl:text-2xl">
            Loading
          </span>
        </div>
      )}
      {!isLoading && products.length > 0 && (
        <div className="flex flex-col">
          <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
              <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table className="min-w-full divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        ID
                      </th>

                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        <div className="w-full flex">
                          <span className="my-auto">TYPE</span>
                          {sortType === "Type ascending" && (
                            <ArrowNarrowUpIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                          {sortType === "Type descending" && (
                            <ArrowNarrowDownIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                        </div>
                      </th>

                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        <div className="w-full flex">
                          <span className="my-auto">CODE</span>
                          {sortType === "Code ascending" && (
                            <ArrowNarrowUpIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                          {sortType === "Code descending" && (
                            <ArrowNarrowDownIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                        </div>
                      </th>

                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        <div className="w-full flex">
                          <span className="my-auto">DESCRIPTION</span>
                          {sortType === "Description ascending" && (
                            <ArrowNarrowUpIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                          {sortType === "Description descending" && (
                            <ArrowNarrowDownIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                        </div>
                      </th>

                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        <div className="w-full flex">
                          <span className="my-auto">NUMBER CODE</span>
                          {sortType === "Number Code ascending" && (
                            <ArrowNarrowUpIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                          {sortType === "Number Code descending" && (
                            <ArrowNarrowDownIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                        </div>
                      </th>

                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        <div className="w-full flex">
                          <span className="my-auto">GROUP</span>
                          {sortType === "Group ascending" && (
                            <ArrowNarrowUpIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                          {sortType === "Group descending" && (
                            <ArrowNarrowDownIcon
                              className="my-auto -mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                              aria-hidden="true"
                            />
                          )}
                        </div>
                      </th>

                      <th></th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {products.map((product, index) =>
                      index >= indexIntervals[0] &&
                      index <= indexIntervals[1] && (
                        <React.Fragment key={product.product_id}>
                          {product.product_description
                            .toLowerCase()
                            .includes(searchBarProduct.toLowerCase()) && (
                              <tr key={product.product_id}>
                                <td className="px-6 py-4 truncate-2 text-sm font-medium text-gray-900">
                                  {product.product_id}
                                </td>
                                <td className="px-6 py-4 truncate-2 text-sm text-gray-500">
                                  {product.product_type}
                                </td>
                                <td className="px-6 py-4 truncate-2 text-sm text-gray-500">
                                  {product.product_code}
                                </td>
                                <td className="px-6 py-4 truncate-2 text-sm text-gray-500 ">
                                  {product.product_description}
                                </td>
                                <td className="px-6 py-4 truncate-2 text-sm text-gray-500">
                                  {product.product_number_code}
                                </td>
                                <td className="px-6 py-4 truncate-2 text-sm text-gray-500">
                                  {product.product_group}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                  <button
                                    onClick={() => {
                                      toggleModal();
                                      getProductInfo(product.product_id);
                                    }}
                                  >
                                    <SearchCircleIcon className="h-6 w-6" />
                                  </button>
                                </td>
                              </tr>
                            )}
                        </React.Fragment>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="flex flex-row justify-center items-center space-x-1">
            {currentPage - 2 >= 1 && (
              <button
                className="bg-slate-200 rounded-sm px-2 py-1"
                onClick={() => setCurrentPage(currentPage - 2)}
              >
                {currentPage - 2}
              </button>
            )}

            {currentPage - 1 >= 1 && (
              <button
                className="bg-slate-200 rounded-sm px-2 py-1"
                onClick={() => setCurrentPage(currentPage - 1)}
              >
                {currentPage - 1}
              </button>
            )}
            <button
              className="bg-slate-400 rounded-sm px-2 py-1"
              onClick={() => setCurrentPage(currentPage)}
            >
              {currentPage}
            </button>
            {currentPage + 1 <= maxPage && (
              <button
                className="bg-slate-200 rounded-sm px-2 py-1"
                onClick={() => setCurrentPage(currentPage + 1)}
              >
                {currentPage + 1}
              </button>
            )}
            {currentPage + 2 <= maxPage && (
              <button
                className="bg-slate-200 rounded-sm px-2 py-1"
                onClick={() => setCurrentPage(currentPage + 2)}
              >
                {currentPage + 2}
              </button>
            )}
            {currentPage + 3 <= maxPage && (
              <>
                <span>...</span>
                <button
                  className="bg-slate-200 rounded-sm px-2 py-1"
                  onClick={() => setCurrentPage(maxPage)}
                >
                  {maxPage}
                </button>
              </>
            )}
          </div>
          {/* //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   */}
          {/* MODAL TO SHOW THE PRODUCT INFORMATION */}
          {isModalOpen && individualProduct && (
            <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center bg-black bg-opacity-30 z-20">
              <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 h-[700px] w-[1000px] bg-white border rounded-md p-3">
                <div className="flex flex-col space-y-4 justify-center items-center">
                  <div className="flex flex-row self-end">
                    <button
                      type="button"
                      className="rounded-md bg-white text-gray-900 focus:ring-offset-2 hover:text-gray-500"
                      onClick={toggleModal}
                    >
                      <XIcon className="h-6 w-6" aria-hidden="true" />
                    </button>
                  </div>
                  <span className="font-bold text-lg">{individualProduct.basicInfo.product_description}</span>
                  {/* FIRST ROW */}
                  <div className="mt-8 gap-6 grid grid-cols-2">
                    <div className="text-sm text-gray-500 dark:text-gray-300 span-col-3">
                      <div className="pt-10">
                        <p className="text-lg  font-bold leading-6 text-gray-900 mb-4">
                          Sales
                        </p>
                        <dl className="grid grid-cols-2 gap-5">
                          <div className="relative overflow-hidden rounded-lg bg-gray-200 px-4 pt-2 shadow sm:px-6 sm:pt-6">
                            <dt>
                              <div className="absolute rounded-md bg-indigo-500 p-3">
                                <CashIcon
                                  className="h-4 w-4 text-white"
                                  aria-hidden="true"
                                />
                              </div>
                              <p className="ml-16 text-lg font-medium text-gray-500 ">
                                Total in Sales
                              </p>
                            </dt>
                            <dd className="ml-16 flex items-baseline pb-6 sm:pb-7">
                              <p className="text-lg font-semibold text-gray-700 ">
                                {individualProduct.total_in_sales}€
                              </p>
                            </dd>
                          </div>

                          <div className="relative overflow-hidden rounded-lg bg-gray-200 px-4 pt-5 shadow  sm:px-6 sm:pt-6">
                            <dt>
                              <div className="absolute rounded-md bg-indigo-500 p-3">
                                <CurrencyEuroIcon
                                  className="h-4 w-4 text-white"
                                  aria-hidden="true"
                                />
                              </div>
                              <p className="ml-16 text-lg font-medium text-gray-500 ">
                                Sales
                              </p>
                            </dt>
                            <dd className="ml-16 flex items-baseline pb-6 sm:pb-7">
                              <p className="text-lg font-semibold text-gray-700 ">
                                {individualProduct.total_sales}
                              </p>
                            </dd>
                          </div>
                        </dl>
                      </div>
                    </div>

                    {/* LOCAL INFO */}
                    <div className="span-col-1 mt-8">
                      <div className="text-sm text-gray-500 dark:text-gray-300">
                        <div className="col-span-4 md:col-span-3">
                          <p className="text-lg  font-bold leading-6 text-gray-900 mb-6">
                            Best Local
                          </p>
                          <table className="min-w-full divide-y divide-gray-200 border border-gray-500">
                            <thead className="bg-gray-300 border border-gray-500">
                              <tr>
                                <th
                                  scope="col"
                                  className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                  City
                                </th>
                                <th
                                  scope="col"
                                  className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                  Sales Value
                                </th>
                              </tr>
                            </thead>
                            <tbody className="bg-gray-100 divide-y divide-gray-400">
                              <tr>
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                  {individualProduct.best_local.city}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                  {individualProduct.best_local.best_local_total}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* SECOND ROW */}
                  <div className="mt-8 gap-6 grid grid-cols-2">
                    {/* INVOICE */}
                    <div className="span-col-1 mt-8">
                      <div className="text-sm text-gray-500 dark:text-gray-300">
                        <div className="col-span-4 md:col-span-3">
                          <p className="text-lg  font-bold leading-6 text-gray-900 mb-4">
                            Latest Product invoices
                          </p>
                          <table className="min-w-full divide-y divide-gray-200 border border-gray-500">
                            <thead className="bg-gray-300 border border-gray-500">
                              <tr>
                                <th
                                  scope="col"
                                  className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                  Date
                                </th>
                                <th
                                  scope="col"
                                  className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                  Invoice number
                                </th>
                                <th
                                  scope="col"
                                  className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                  Customer ID
                                </th>
                                <th
                                  scope="col"
                                  className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                  Gross Total
                                </th>
                              </tr>
                            </thead>
                            <tbody className="bg-gray-100 divide-y divide-gray-400">
                              {(() => {
                                const rows = [];
                                for (
                                  let i = 0;
                                  i < individualProduct.last_product_sales.length;
                                  i++
                                ) {
                                  if (i >= 4) {
                                    break;
                                  }
                                  const invoice = individualProduct.last_product_sales[i];
                                  rows.push(
                                    <tr key={invoice.invoice_no}>
                                      <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                        {new Date(invoice.invoice_date).toLocaleDateString(
                                          "en-GB"
                                        )}
                                      </td>
                                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {invoice.invoice_no}
                                      </td>
                                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {invoice.customer_id}
                                      </td>
                                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {invoice.gross_total}
                                      </td>
                                    </tr>
                                  );
                                }
                                return rows;
                              })()}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                    <div className="span-col-1 mt-9">
                      <div className="text-sm text-gray-500 dark:text-gray-300">
                        <div className="col-span-4 md:col-span-3">
                          <p className="text-lg  font-bold leading-6 text-gray-900 mb-4">
                            Products Sales
                          </p>
                          <Line
                            options={options}
                            responsive="true"
                            data={generateLineChartData([
                              {
                                label: "Product Sales",
                                values: Object.values(dashboardValues),
                                pointBorderColor: "#4495e2",
                                pointHoverBackgroundColor: "#4495e2",
                                borderColor: "#4495e2",
                                backgroundColor: "#4495e2",
                              },
                            ])}
                          />
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      )}
      {!isLoading && products.length === 0 && (
        <h2 className="text-center text-lg font-extrabold m-5">No products found</h2>
      )}
    </div>
  );
};

export default Products;

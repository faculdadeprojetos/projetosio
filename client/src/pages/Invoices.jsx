import { useEffect, useState } from "react";
import ReactLoading from "react-loading";
import { useNavigate } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import { XIcon, ChevronUpIcon, ChevronDownIcon, SearchCircleIcon } from "@heroicons/react/outline";
const Invoices = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [invoices, setInvoices] = useState([]);
  const [invoicesToShow, setInvoicesToShow] = useState([]);
  const [numOfItems, setNumOfItems] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [indexIntervals, setIndexIntervals] = useState();
  const [maxPage, setMaxPage] = useState();
  const [searchByNumber, setSearchByNumber] = useState("");
  const navigate = useNavigate();

  const [filters, setFilters] = useState({
    customer: { selected: false, value: 0 },
    dateInterval: { selected: false, lb: "", ub: "  " },
    invoiceType: { selected: false, ft: false, fr: false, fs: false },
    taxPayable: { selected: false, lb: 0, ub: 0 },
    netTotal: { selected: false, lb: 0, ub: 0 },
    grossTotal: { selected: false, lb: 0, ub: 0 },
  });
  const [isModalOpen, setIsModelOpen] = useState(false);
  const [currentInvoiceLines, setCurrentInvoiceLines] = useState([]);

  const fetchInvoiceLines = (invoiceNo) => {
    const token = localStorage.getItem("accessToken");
    if (token) {
      fetch(`http://localhost:4000/invoices/invoiceLines?invoiceNo=${invoiceNo}`, {
        headers: { Authorization: token },
      })
        .then((data) => {
          data.json().then((json) => {
            if (json.error) {
              toast.error("Server error while fetching data");
            } else {
              setCurrentInvoiceLines(json.invoiceLines);
              console.log(json.invoiceLines);
            }
          });
        })
        .catch((err) => {
          toast.error("Server error while fetching data");
        });
    } else {
      toast.error("Auth expired");
      navigate("/")
    }
  };

  const applyFilters = () => {
    if (
      !filters.customer.selected &&
      !filters.dateInterval.selected &&
      !filters.invoiceType.selected &&
      !filters.taxPayable.selected &&
      !filters.netTotal.selected &&
      !filters.grossTotal.selected
    ) {
      toast.error("No filters to apply");
      return;
    }

    var tmp = invoices;

    if (filters.customer.selected) {
      tmp = tmp.filter((invoice) => invoice.customer_id == filters.customer.value);
    }

    if (filters.dateInterval.selected) {
      if (filters.dateInterval.lb && !filters.dateInterval.ub) {
        tmp = tmp.filter((invoice) => invoice.invoice_date >= filters.dateInterval.lb);
      } else if (!filters.dateInterval.lb && filters.dateInterval.ub) {
        tmp = tmp.filter((invoice) => invoice.invoice_date <= filters.dateInterval.ub);
      } else if (!filters.dateInterval.lb && !filters.dateInterval.ub) {
        toast.error("Invalid date interval");
        return;
      } else {
        tmp = tmp.filter(
          (invoice) =>
            invoice.invoice_date >= filters.dateInterval.lb &&
            invoice.invoice_date <= filters.dateInterval.ub
        );
      }
    }

    if (filters.invoiceType.selected) {
      var tmp2 = [];
      if (!filters.invoiceType.ft && !filters.invoiceType.fr && !filters.invoiceType.fs) {
        toast.error("Select at least one invoice type");
        return;
      }

      if (filters.invoiceType.ft) {
        var ft = tmp.filter((invoice) => invoice.invoice_type === "FT");
        tmp2.push(...ft);
      }
      if (filters.invoiceType.fr) {
        var fr = tmp.filter((invoice) => invoice.invoice_type === "FR");
        tmp2.push(...fr);
      }
      if (filters.invoiceType.fs) {
        var fs = tmp.filter((invoice) => invoice.invoice_type === "FS");
        tmp2.push(...fs);
      }
      tmp = tmp2;
    }

    if (filters.taxPayable.selected) {
      if (filters.taxPayable.ub < filters.taxPayable.lb && filters.taxPayable.ub != 0) {
        toast.error("Invalid tax payable interval");
        return;
      }
      if (filters.taxPayable.ub < filters.taxPayable.lb && filters.taxPayable.ub == 0) {
        tmp = tmp.filter((invoice) => parseFloat(invoice.tax_payable) >= filters.taxPayable.lb);
      } else {
        tmp = tmp.filter(
          (invoice) =>
            parseFloat(invoice.tax_payable) >= filters.taxPayable.lb &&
            parseFloat(invoice.tax_payable) <= filters.taxPayable.ub
        );
      }
    }

    if (filters.netTotal.selected) {
      if (filters.netTotal.ub < filters.netTotal.lb && filters.netTotal.ub != 0) {
        toast.error("Invalid net total interval");
        return;
      }
      if (filters.netTotal.ub < filters.netTotal.lb && filters.netTotal.ub == 0) {
        tmp = tmp.filter((invoice) => parseFloat(invoice.net_total) >= filters.netTotal.lb);
      } else {
        tmp = tmp.filter(
          (invoice) =>
            parseFloat(invoice.net_total) >= filters.netTotal.lb &&
            parseFloat(invoice.net_total) <= filters.netTotal.ub
        );
      }
    }

    if (filters.grossTotal.selected) {
      if (filters.grossTotal.ub < filters.grossTotal.lb && filters.grossTotal.ub != 0) {
        toast.error("Invalid gross total interval");
        return;
      }
      if (filters.grossTotal.ub < filters.grossTotal.lb && filters.grossTotal.ub == 0) {
        tmp = tmp.filter((invoice) => parseFloat(invoice.gross_total) >= filters.grossTotal.lb);
      } else {
        tmp = tmp.filter(
          (invoice) =>
            parseFloat(invoice.gross_total) >= filters.grossTotal.lb &&
            parseFloat(invoice.gross_total) <= filters.grossTotal.ub
        );
      }
    }

    setInvoicesToShow(tmp);
  };

  const clearFilters = () => {
    setFilters({
      customer: { selected: false, value: 0 },
      dateInterval: { selected: false, lb: "", ub: "" },
      invoiceType: { selected: false, ft: false, fr: false, fs: false },
      taxPayable: { selected: false, lb: 0, ub: 0 },
      netTotal: { selected: false, lb: 0, ub: 0 },
      grossTotal: { selected: false, lb: 0, ub: 0 },
    });
    setInvoicesToShow(invoices);
  };

  useEffect(() => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      if (searchByNumber.trim().length === 0) {
        setInvoicesToShow(invoices);
      } else {
        const tmp = invoicesToShow;
        setInvoicesToShow(
          tmp.filter((invoice) =>
            invoice.invoice_no
              .toLowerCase()
              .replace(/\s/g, "")
              .includes(searchByNumber.toLowerCase().replace(/\s/g, ""))
          )
        );
      }
    } else {
      toast.error("Session expired")
      navigate("/")
    }
  }, [searchByNumber]);

  useEffect(() => {
    setInvoicesToShow(invoices);
  }, [invoices]);

  useEffect(() => {
    setMaxPage(Math.floor(invoicesToShow.length / numOfItems));
    setCurrentPage(1);
  }, [invoicesToShow, numOfItems]);

  useEffect(() => {
    const end = currentPage * numOfItems - 1;
    const start = end - numOfItems + 1;
    setIndexIntervals([start, end]);
  }, [currentPage, numOfItems]);

  useEffect(() => {
    const token = localStorage.getItem("accessToken");
    if (token) {
      fetch("http://localhost:4000/invoices", { headers: { Authorization: token } })
        .then((data) => {
          data.json().then((json) => {
            if (data.ok) {
              setIsLoading(false);
              setInvoices(json.invoices);
            } else {
              setIsLoading(false);
              toast.error("Server error while fetching data");
            }
          });
        })
        .catch((err) => {
          toast.error("Server error while fetching data");
        });
    } else {
      toast.error("Auth expired");
    }
  }, []);

  return (
    <div className="my-3">
      <Toaster />
      <div className="px-4 sm:px-6 md:px-0 mb-4 mt-2">
        <h1 className="text-2xl font-semibold text-gray-900">Invoices</h1>
      </div>

      <div className="flex flex-col space-y-2">
        <div className="flex flex-row justify-end space-x-1 items-center">
          <input
            value={searchByNumber}
            type="text"
            placeholder="search by number"
            className="rounded-md h-8"
            onChange={(event) => {
              setSearchByNumber(event.target.value);
            }}
          />
          <select
            className="text-xs rounded-md h-8"
            onChange={(event) => {
              setNumOfItems(event.target.value);
            }}
          >
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
          </select>
        </div>

        <div className="flex flex-col space-y-6 bg-slate-50 border rounded-md px-4 py-5 justify-start items-center">
          <div className="flex flex-row items-center self-start space-x-6">
            <div className="flex flex-row items-center space-x-2">
              <input
                type="checkbox"
                checked={filters.customer.selected}
                onChange={() => {
                  const updatedFilters = { ...filters };
                  updatedFilters.customer.selected = !updatedFilters.customer.selected;
                  setFilters(updatedFilters);
                }}
                className="rounded-sm focus:ring-0 checked:ring-1"
              />
              <span className="text-xs font-semibold">CUSTOMER:</span>
              <input
                type="number"
                value={filters.customer.value}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.customer.value = event.target.value;
                  setFilters(updatedFilters);
                }}
                min={0}
                className="h-7 rounded-md w-20"
              />
            </div>
            <div className="flex flex-row items-center space-x-2">
              <input
                checked={filters.dateInterval.selected}
                onChange={() => {
                  const updatedFilters = { ...filters };
                  updatedFilters.dateInterval.selected = !updatedFilters.dateInterval.selected;
                  setFilters(updatedFilters);
                }}
                type="checkbox"
                className="rounded-sm focus:ring-0 checked:ring-1"
              />
              <span className="text-xs font-semibold">DATE INTERVAL:</span>
              <input
                max={filters.dateInterval.ub}
                type="date"
                className="h-7 rounded-md w-36"
                value={filters.dateInterval.lb}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.dateInterval.lb = event.target.value;
                  setFilters(updatedFilters);
                }}
              />
              <input
                min={filters.dateInterval.lb}
                type="date"
                className="h-7 rounded-md w-36"
                value={filters.dateInterval.ub}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.dateInterval.ub = event.target.value;
                  setFilters(updatedFilters);
                }}
              />
            </div>
            <div className="flex flex-row items-center space-x-2">
              <input
                checked={filters.invoiceType.selected}
                onChange={() => {
                  const updatedFilters = { ...filters };
                  updatedFilters.invoiceType.selected = !updatedFilters.invoiceType.selected;
                  setFilters(updatedFilters);
                }}
                type="checkbox"
                className="rounded-sm focus:ring-0 checked:ring-1"
              />
              <span className="text-xs font-semibold">INVOICE TYPE:</span>
              <div className="flex flex-row space-x-0">
                <button
                  className={`rounded-l-md bg-slate-3 ${filters.invoiceType.ft ? "bg-cyan-600" : "bg-gray-300"
                    } px-2 py-1 text-white`}
                  value={filters.invoiceType.ft}
                  onClick={() => {
                    const updatedFilters = { ...filters };
                    updatedFilters.invoiceType.ft = !updatedFilters.invoiceType.ft;
                    setFilters(updatedFilters);
                  }}
                >
                  FT
                </button>
                <button
                  className={`bg-slate-3 ${filters.invoiceType.fr ? "bg-cyan-600" : "bg-gray-300"
                    } px-2 py-1 text-white`}
                  value={filters.invoiceType.fr}
                  onClick={() => {
                    const updatedFilters = { ...filters };
                    updatedFilters.invoiceType.fr = !updatedFilters.invoiceType.fr;
                    setFilters(updatedFilters);
                  }}
                >
                  FR
                </button>
                <button
                  className={`rounded-r-md bg-slate-3 ${filters.invoiceType.fs ? "bg-cyan-600" : "bg-gray-300"
                    } px-2 py-1 text-white`}
                  value={filters.invoiceType.fs}
                  onClick={() => {
                    const updatedFilters = { ...filters };
                    updatedFilters.invoiceType.fs = !updatedFilters.invoiceType.fs;
                    setFilters(updatedFilters);
                  }}
                >
                  FS
                </button>
              </div>
            </div>
          </div>
          <div className="flex flex-row items-center self-start space-x-6">
            <div className="flex flex-row items-center space-x-2">
              <input
                type="checkbox"
                checked={filters.taxPayable.selected}
                onChange={() => {
                  const updatedFilters = { ...filters };
                  updatedFilters.taxPayable.selected = !updatedFilters.taxPayable.selected;
                  setFilters(updatedFilters);
                }}
                className="rounded-sm focus:ring-0 checked:ring-1"
              />
              <span className="text-xs font-semibold">TAX PAYABLE INTERVAL:</span>
              <input
                type="number"
                value={filters.taxPayable.lb}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.taxPayable.lb = event.target.value;
                  setFilters(updatedFilters);
                }}
                min={0}
                className="h-7 rounded-md w-20"
              />
              <input
                type="number"
                value={filters.taxPayable.ub}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.taxPayable.ub = event.target.value;
                  setFilters(updatedFilters);
                }}
                min={0}
                className="h-7 rounded-md w-20"
              />
            </div>
            <div className="flex flex-row items-center space-x-2">
              <input
                type="checkbox"
                checked={filters.netTotal.selected}
                onChange={() => {
                  const updatedFilters = { ...filters };
                  updatedFilters.netTotal.selected = !updatedFilters.netTotal.selected;
                  setFilters(updatedFilters);
                }}
                className="rounded-sm focus:ring-0 checked:ring-1"
              />
              <span className="text-xs font-semibold">NET TOTAL INTERVAL:</span>
              <input
                type="number"
                value={filters.netTotal.lb}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.netTotal.lb = event.target.value;
                  setFilters(updatedFilters);
                }}
                min={0}
                className="h-7 rounded-md w-20"
              />
              <input
                type="number"
                value={filters.netTotal.ub}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.netTotal.ub = event.target.value;
                  setFilters(updatedFilters);
                }}
                min={0}
                className="h-7 rounded-md w-20"
              />
            </div>
            <div className="flex flex-row items-center space-x-2">
              <input
                type="checkbox"
                checked={filters.grossTotal.selected}
                onChange={() => {
                  const updatedFilters = { ...filters };
                  updatedFilters.grossTotal.selected = !updatedFilters.grossTotal.selected;
                  setFilters(updatedFilters);
                }}
                className="rounded-sm focus:ring-0 checked:ring-1"
              />
              <span className="text-xs font-semibold">GROSS TOTAL INTERVAL:</span>
              <input
                type="number"
                value={filters.grossTotal.lb}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.grossTotal.lb = event.target.value;
                  setFilters(updatedFilters);
                }}
                min={0}
                className="h-7 rounded-md w-20"
              />
              <input
                type="number"
                value={filters.grossTotal.ub}
                onChange={(event) => {
                  const updatedFilters = { ...filters };
                  updatedFilters.grossTotal.ub = event.target.value;
                  setFilters(updatedFilters);
                }}
                min={0}
                className="h-7 rounded-md w-20"
              />
            </div>
          </div>
          <div className="flex flex-row space-x-2 justify-center text-white">
            <button
              className=" bg-cyan-600 py-1 px-2 drop-shadow-md hover:bg-cyan-700 active:bg-cyan-800"
              onClick={() => clearFilters()}
            >
              CLEAR ALL
            </button>
            <button
              className=" bg-cyan-600 py-1 px-2 drop-shadow-md hover:bg-cyan-700 active:bg-cyan-800"
              onClick={() => applyFilters()}
            >
              APPLY FILTERS
            </button>
          </div>
        </div>

        {isLoading && (
          <div className="flex flex-wrap items-center justify-center pt-24 ">
            <ReactLoading type={"bubbles"} height={100} width={120} color={"#5b2bab"} />
            <span className="mb-4 w-full self-start text-center text-2xl text-lg font-extrabold tracking-tight dark:text-white xl:text-2xl">
              Loading
            </span>
          </div>
        )}
        {!isLoading && invoicesToShow.length > 0 && (
          <div className="flex flex-col space-y-2">
            <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table className="min-w-full divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          NUMBER
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          <div className="flex flex-row space-x-2 items-center">
                            <span>DATE</span>

                            <div className="flex flex-col space-y-0">
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    const dateA = new Date(a.invoice_date);
                                    const dateB = new Date(b.invoice_date);
                                    return dateA - dateB;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronUpIcon className="h-5 w-3" />
                              </button>
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    const dateA = new Date(a.invoice_date);
                                    const dateB = new Date(b.invoice_date);
                                    return dateB - dateA;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronDownIcon className="h-5 w-3" />
                              </button>
                            </div>
                          </div>
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          <div className="flex flex-row space-x-2 items-center">
                            <span>CUSTOMER</span>

                            <div className="flex flex-col space-y-0">
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    return a.customer_id - b.customer_id;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronUpIcon className="h-5 w-3" />
                              </button>
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    return b.customer_id - a.customer_id;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronDownIcon className="h-5 w-3" />
                              </button>
                            </div>
                          </div>
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          TYPE
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          <div className="flex flex-row space-x-2 items-center">
                            <span>TAX PAYABLE</span>

                            <div className="flex flex-col space-y-0">
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    return a.tax_payable - b.tax_payable;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronUpIcon className="h-5 w-3" />
                              </button>
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    return b.tax_payable - a.tax_payable;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronDownIcon className="h-5 w-3" />
                              </button>
                            </div>
                          </div>
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          <div className="flex flex-row space-x-2 items-center">
                            <span>NET TOTAL</span>

                            <div className="flex flex-col space-y-0">
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    return a.net_total - b.net_total;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronUpIcon className="h-5 w-3" />
                              </button>
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    return b.net_total - a.net_total;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronDownIcon className="h-5 w-3" />
                              </button>
                            </div>
                          </div>
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          <div className="flex flex-row space-x-2 items-center">
                            <span>GROSS TOTAL</span>

                            <div className="flex flex-col space-y-0">
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    return a.gross_total - b.gross_total;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronUpIcon className="h-5 w-3" />
                              </button>
                              <button
                                onClick={() => {
                                  const sortedInvoices = [...invoicesToShow].sort((a, b) => {
                                    return b.gross_total - a.gross_total;
                                  });
                                  setInvoicesToShow(sortedInvoices);
                                }}
                              >
                                <ChevronDownIcon className="h-5 w-3" />
                              </button>
                            </div>
                          </div>
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        ></th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200">
                      {invoicesToShow.map(
                        (invoice, index) =>
                          index >= indexIntervals[0] &&
                          index <= indexIntervals[1] && (
                            <tr key={invoice.invoice_no}>
                              <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {invoice.invoice_no}
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {invoice.invoice_date.split("T")[0]}
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {invoice.customer_id}
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {invoice.invoice_type}
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {invoice.tax_payable}€
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {invoice.net_total}€
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {invoice.gross_total}€
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                <button
                                  onClick={() => {
                                    setIsModelOpen(true);
                                    fetchInvoiceLines(invoice.invoice_no);
                                  }}
                                >
                                  <SearchCircleIcon className="h-6 w-6" />
                                </button>
                              </td>
                            </tr>
                          )
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="flex flex-row justify-center items-center space-x-1">
              {currentPage - 2 >= 1 && (
                <button
                  className="bg-slate-200 rounded-sm px-2 py-1"
                  onClick={() => setCurrentPage(currentPage - 2)}
                >
                  {currentPage - 2}
                </button>
              )}

              {currentPage - 1 >= 1 && (
                <button
                  className="bg-slate-200 rounded-sm px-2 py-1"
                  onClick={() => setCurrentPage(currentPage - 1)}
                >
                  {currentPage - 1}
                </button>
              )}
              <button
                className="bg-slate-400 rounded-sm px-2 py-1"
                onClick={() => setCurrentPage(currentPage)}
              >
                {currentPage}
              </button>
              {currentPage + 1 <= maxPage && (
                <button
                  className="bg-slate-200 rounded-sm px-2 py-1"
                  onClick={() => setCurrentPage(currentPage + 1)}
                >
                  {currentPage + 1}
                </button>
              )}
              {currentPage + 2 <= maxPage && (
                <button
                  className="bg-slate-200 rounded-sm px-2 py-1"
                  onClick={() => setCurrentPage(currentPage + 2)}
                >
                  {currentPage + 2}
                </button>
              )}
              {currentPage + 3 <= maxPage && (
                <>
                  <span>...</span>
                  <button
                    className="bg-slate-200 rounded-sm px-2 py-1"
                    onClick={() => setCurrentPage(maxPage)}
                  >
                    {maxPage}
                  </button>
                </>
              )}
            </div>
          </div>
        )}
        {!isLoading && invoicesToShow.length === 0 && (
          <h2 className="text-center text-lg font-extrabold m-5">No invoices found</h2>
        )}
      </div>
      {isModalOpen && (
        <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center bg-black bg-opacity-30 z-20">
          <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 h-[700px] w-[1000px] bg-white border rounded-md p-3">
            <div className="flex flex-col space-y-4 justify-center items-center">
              <div className="flex flex-row self-end">
                <button
                  type="button"
                  className="rounded-md bg-white text-gray-900 focus:ring-offset-2 hover:text-gray-500"
                  onClick={() => setIsModelOpen(false)}
                >
                  <XIcon className="h-6 w-6" aria-hidden="true" />
                </button>
              </div>
              <span className="font-bold text-lg">INVOICE LINES</span>
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      LINE
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      QUANTITY
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      DESCRIPTION
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      P. Unit value
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Tax
                    </th>
                  </tr>
                </thead>
                {currentInvoiceLines.length > 0 ? (
                  <tbody>
                    {currentInvoiceLines.map((line, index) => (
                      <tr>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {index + 1}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {line.quantity} {line.unit_measure}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {line.product_description}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {line.unit_price} €
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {line.tax_percentage} %
                        </td>
                      </tr>
                    ))}
                  </tbody>
                ) : null}
              </table>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Invoices;

import { useEffect, useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import ReactLoading from "react-loading";
import {
  Widgets,
  SalesCharts,
  ClientCharts,
  ProductCharts,
  ServicesCharts,
  FamilyGroupsCharts,
} from "../components";
import { ChevronDownIcon, ChevronUpIcon, SearchCircleIcon } from "@heroicons/react/outline";
import { getMonthYearRange, formatDate } from "../helper/helper";
/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";

const Dashboard = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [hasClients, setHasClients] = useState(false);
  const [stats, setStats] = useState({});
  const [generalInfo, setGeneralInfo] = useState({});
  const [dateRange, setDateRange] = useState([]);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [showingStartDate, setShowingStartDate] = useState("");
  const [showingEndDate, setShowingEndDate] = useState("");

  const [showSales, setShowSales] = useState(true);
  const [showClients, setShowClients] = useState(true);
  const [showProducts, setShowProducts] = useState(true);
  const [showServices, setShowServices] = useState(true);
  const [showProductFamilies, setShowProductFamilies] = useState(true);
  const navigate = useNavigate();
  function searchStats() {
    const [start_month, start_year] = startDate.split("-").map(Number);
    const [end_month, end_year] = endDate.split("-").map(Number);

    if (start_year < end_year) {
    } else if (start_year === end_year && start_month < end_month) {
    } else {
      toast.error("The start date must be lower than end date");
      return false;
    }

    let modifiedStartDate = `${start_year}-${start_month}-01`;

    //check the days that month has
    let day = 28
    if (end_month === 2) {
      day = 28;
    } else if ([4, 6, 9, 11].includes(end_month)) {
      day = 30;
    } else if ([1, 3, 5, 7, 8, 10, 12].includes(end_month)) {
      day = 31;
    }

    let modifiedEndDate = `${end_year}-${end_month}-${day}`;

    const token = localStorage.getItem("accessToken");

    if (token) {
      setIsLoading(true);
      fetch(`http://localhost:4000/stats?startDate=${modifiedStartDate}&endDate=${modifiedEndDate}`, { "headers": { Authorization: token } })
        .then((data) => {
          data.json().then((json) => {
            if (data.ok) {
              if (json.bestClientsValue.length > 0) {
                console.log(json);
                setStats(json);
                setShowingStartDate(startDate);
                setShowingEndDate(endDate);
                setHasClients(true);
              }
            } else {
              notifyError();
            }
            setIsLoading(false);
          });
        })
        .catch((err) => {
          setIsLoading(false);
          notifyError();
        });
    } else {
      // IMPLEMENT THIS BLOCK
      toast.error("Session expired")
      navigate("/")
    }
  }

  const notifyError = () => toast.error("Error trying to get data! Try again later");

  console.log(dateRange);

  useEffect(() => {
    setIsLoading(true);
    const token = localStorage.getItem("accessToken");

    if (token) {

      fetch(`http://localhost:4000/stats?startDate=${startDate}&endDate=${endDate}`, { "headers": { Authorization: token } })
        .then((data) => {
          data.json().then((json) => {
            if (data.ok) {
              if (json.bestClientsValue.length > 0) {
                console.log(json);
                setStats(json);
                if (generalInfo.fiscal_year === undefined) {
                  setGeneralInfo(json.generalInfo);
                  let arr = getMonthYearRange(json.generalInfo.start_date, json.generalInfo.end_date);
                  setDateRange(arr);
                  setStartDate(arr[0]);
                  setEndDate(arr[arr.length - 1]);
                  setShowingStartDate(arr[0]);
                  setShowingEndDate(arr[arr.length - 1]);
                }
                setHasClients(true);
              }
            } else {
              notifyError();
            }
            setIsLoading(false);
          });
        })
        .catch((err) => {
          setIsLoading(false);
          notifyError();
        });
    } else {
      toast.error("Session expired")
      navigate("/")
    }
  }, []);

  return (
    <div>
      <Toaster />
      <div className="px-4 sm:px-6 md:px-0 mt-2">
        <h1 className="text-2xl font-semibold text-gray-900 mb-1">Dashboard</h1>
        {!isLoading && hasClients && (
          <>
            <h3 className="text-md font-semibold text-gray-500">
              Showing temporal range: {formatDate(showingStartDate)} / {formatDate(showingEndDate)}
            </h3>
            <h3 className="text-md font-semibold text-gray-500 mb-4">
              Currency code: {generalInfo.currency_code}
            </h3>
          </>
        )}
      </div>
      {isLoading && (
        <div className="flex flex-wrap items-center justify-center pt-24 ">
          <ReactLoading type={"bubbles"} height={100} width={120} color={"#5b2bab"} />
          <span className="mb-4 w-full self-start text-center text-2xl text-lg font-extrabold tracking-tight dark:text-white xl:text-2xl">
            Loading data
          </span>
        </div>
      )}
      {/* filter of time */}
      {!isLoading && hasClients && (
        <div className="w-full flex justify-end">
          <Menu as="div" className="relative inline-block text-left">
            <div>
              <Menu.Button className="inline-flex justify-center z-20 w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 z-0">
                Start date
                <ChevronDownIcon className="-mr-1 ml-2 h-5 w-5" aria-hidden="true" />
              </Menu.Button>
            </div>

            <Transition
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="origin-top-right bg-gray-100 z-60 absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="py-1">
                  {dateRange.map((date, key) => (
                    <Menu.Item key={key}>
                      <p
                        onClick={() => {
                          setStartDate(date);
                        }}
                        className={`${startDate === date ? "font-extrabold text-gray-900" : "text-gray-700"
                          }
                            block cursor-pointer px-4 py-2 text-sm`}
                      >
                        {formatDate(date)}
                      </p>
                    </Menu.Item>
                  ))}
                </div>
              </Menu.Items>
            </Transition>
          </Menu>
          <Menu as="div" className="relative inline-block text-left mx-5">
            <div>
              <Menu.Button className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 z-20">
                End date
                <ChevronDownIcon className="-mr-1 ml-2 h-5 w-5" aria-hidden="true" />
              </Menu.Button>
            </div>

            <Transition
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="origin-top-right bg-gray-100 absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="py-1">
                  {dateRange.map((date, key) => (
                    <Menu.Item key={key}>
                      <p
                        onClick={() => {
                          setEndDate(date);
                        }}
                        className={`${endDate === date ? "font-extrabold text-gray-900" : "text-gray-700"
                          }
                            block cursor-pointer px-4 py-2 text-sm`}
                      >
                        {formatDate(date)}
                      </p>
                    </Menu.Item>
                  ))}
                </div>
              </Menu.Items>
            </Transition>
          </Menu>

          <button
            class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center"
            onClick={() => {
              searchStats();
            }}
          >
            <SearchCircleIcon className="w-5 h-5" />
            <span className="ml-4">Search</span>
          </button>
        </div>
      )}
      {!isLoading && !hasClients && (
        <div className="flex p-4 mb-4 mt-5 text-sm text-red-800 rounded-lg bg-red-50 " role="alert">
          <svg
            aria-hidden="true"
            className="flex-shrink-0 inline w-5 h-5 mr-3"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
              clip-rule="evenodd"
            ></path>
          </svg>
          <span className="sr-only">Danger</span>
          <div>
            <span className="font-medium">
              <p>This project only make sense with data inserted via .xml file (SAFT-T)</p> To use
              all the available functionalities of this dashboard, make sure that these requirements
              are met:
            </span>
            <ul className="mt-1.5 ml-4 list-disc list-inside">
              <li>A valid SAFT-T was uploaded</li>
              <li>The SAFT-T uploaded has a minimum of one customer and one invoice</li>
              <li>All customers and products referred by Id, are present on the SAFT</li>
            </ul>
          </div>
        </div>
      )}
      {!isLoading && hasClients && (
        <>
          {/* Widgets include the total gross, total net, number of clients (total and active) and avg sales value*/}
          <Widgets generalStats={stats.generalStats} />

          {/* Sales charts include mounthly evolution by sales value and number of sales(line), 
          sales by city (map), top 5 cities on number (polar) and value of sales (bar)*/}
          <hr class="mb-6 mt-12 h-0.5 border-t-0 bg-gray-800 opacity-30 z-10" />
          <div className="w-full flex justify-between">
            <h1 className="text-2xl font-semibold text-gray-700 mb-4">Sales</h1>
            <button
              onClick={() => {
                setShowSales(!showSales);
              }}
            >
              {showSales ? (
                <ChevronDownIcon className="text-md h-6 w-6"></ChevronDownIcon>
              ) : (
                <ChevronUpIcon className="text-md h-6 w-6"></ChevronUpIcon>
              )}
            </button>
          </div>
          {showSales && <SalesCharts stats={stats} />}

          {/* Client charts include clients by their billing address (map), top 5 clients on value (bar) 
          and number of sales (doughut) */}
          <hr class="my-6 h-0.5 border-t-0 bg-gray-800 opacity-30" />
          <div className="w-full flex justify-between">
            <h1 className="text-2xl font-semibold text-gray-700 mb-4">Clients</h1>
            <button
              onClick={() => {
                setShowClients(!showClients);
              }}
            >
              {showClients ? (
                <ChevronDownIcon className="text-md h-6 w-6"></ChevronDownIcon>
              ) : (
                <ChevronUpIcon className="text-md h-6 w-6"></ChevronUpIcon>
              )}
            </button>
          </div>
          {showClients && <ClientCharts stats={stats} />}

          {/* Product charts inclue number of sales by product (pie) and the top 5 most selled products*/}
          <hr class="my-6 h-0.5 border-t-0 bg-gray-800 opacity-30" />
          <div className="w-full flex justify-between">
            <h1 className="text-2xl font-semibold text-gray-700 mb-4">Products</h1>
            <button
              onClick={() => {
                setShowProducts(!showProducts);
              }}
            >
              {showProducts ? (
                <ChevronDownIcon className="text-md h-6 w-6"></ChevronDownIcon>
              ) : (
                <ChevronUpIcon className="text-md h-6 w-6"></ChevronUpIcon>
              )}
            </button>
          </div>
          {showProducts && <ProductCharts stats={stats} />}

          {/* Service charts inclue number of sales by services (pie) and the top 5 most selled services*/}
          <hr class="my-6 h-0.5 border-t-0 bg-gray-800 opacity-30" />
          <div className="w-full flex justify-between">
            <h1 className="text-2xl font-semibold text-gray-700 mb-4">Services</h1>
            <button
              onClick={() => {
                setShowServices(!showServices);
              }}
            >
              {showServices ? (
                <ChevronDownIcon className="text-md h-6 w-6"></ChevronDownIcon>
              ) : (
                <ChevronUpIcon className="text-md h-6 w-6"></ChevronUpIcon>
              )}
            </button>
          </div>
          {showServices && <ServicesCharts stats={stats} />}

          {/* FamilyGroup charts inclue number of sales by family group (pie) and the top 5 most selled 
          family groups*/}
          <hr class="my-6 h-0.5 border-t-0 bg-gray-800 opacity-30" />
          <div className="w-full flex justify-between">
            <h1 className="text-2xl font-semibold text-gray-700 mb-4">Product family group</h1>
            <button
              onClick={() => {
                setShowProductFamilies(!showProductFamilies);
              }}
            >
              {showProductFamilies ? (
                <ChevronDownIcon className="text-md h-6 w-6"></ChevronDownIcon>
              ) : (
                <ChevronUpIcon className="text-md h-6 w-6"></ChevronUpIcon>
              )}
            </button>
          </div>
          {showProductFamilies && <FamilyGroupsCharts stats={stats} />}
        </>
      )}
    </div>
  );
};

export default Dashboard;

export function formatDate(dateStr) {
    // Split the date string into month and year components
    const [month, year] = dateStr.split("-")

    // Define an array of Portuguese month names
    const months = [
        "JAN",
        "FEV",
        "MAR",
        "ABR",
        "MAI",
        "JUN",
        "JUL",
        "AGO",
        "SET",
        "OUT",
        "NOV",
        "DEZ",
    ]

    // Convert the month component to an index and retrieve the corresponding month name
    const monthName = months[Number(month) - 1]

    // Return the formatted date string
    return `${monthName}-${year - 2000}`
}

//number: 1-7
export function numberToDayWeek(number) {
    const daysOfWeek = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ]
    return daysOfWeek[number - 1]
}

//convert a string in DD-MM-YYYY to date
export function convertStringToDate(dateStr) {
    let dateParts = dateStr.split("-")
    let day = parseInt(dateParts[2], 10)
    let month = parseInt(dateParts[1], 10) - 1 // Months are zero-based
    let year = parseInt(dateParts[0], 10)

    return new Date(year, month, day)
}

//given a start and end date gives the MONTH-YEAR of all range
export function getMonthYearRange(startDateStr, endDateStr) {
    let dateArray = []
    let startDate = convertStringToDate(startDateStr)
    let endDate = convertStringToDate(endDateStr)

    let currentDate = startDate

    while (currentDate <= endDate) {
        // Extract the month and year
        let month = currentDate.getMonth() + 1 // Months are zero-based, so we add 1
        let year = currentDate.getFullYear()

        // Add month-year to the array
        dateArray.push(`${month}-${year}`)

        // Move to the next month
        currentDate.setMonth(currentDate.getMonth() + 1)
    }

    return dateArray
}

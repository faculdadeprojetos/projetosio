import React from "react";
import ReactDOM from "react-dom/client";
import "tailwindcss/tailwind.css";
import { BrowserRouter as Router } from "react-router-dom";
import { AppRouter } from "./routers";


const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Router>
      <AppRouter />
    </Router>
  </React.StrictMode>
);

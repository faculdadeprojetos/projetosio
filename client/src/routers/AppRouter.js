import { Routes, Route } from "react-router-dom";
import { Dashboard, Invoices, Clients, Products, Taxes, Auth } from "../pages";
import { Template } from "../layout";
import { useLocation } from "react-router-dom";
import Register from "../pages/Register";

const AppRouter = () => {
  const location = useLocation();
  console.log(location);
  return (
    <>
      {location.pathname === "/" || location.pathname === "/register" ? (
        <Routes path="/">
          <Route index element={<Auth />} />
          <Route path="register" element={<Register />} />
        </Routes>
      ) : (
        <Template>
          <Routes>
            <Route path="/app" element={<Dashboard />} />
            <Route path="/app/invoices" element={<Invoices />} />
            <Route path="/app/clients" element={<Clients />} />
            <Route path="/app/products" element={<Products />} />
            <Route path="/app/taxes" element={<Taxes />} />
          </Routes>
        </Template>
      )}
    </>
  );
};

export default AppRouter;

import { InformationCircleIcon } from "@heroicons/react/outline";

const Widgets = ({ generalStats }) => {
  const total_gross = (generalStats.total_gross / 1000).toFixed(3) + "k";
  const total_net = (generalStats.total_net / 1000).toFixed(3) + "k";
  
  return (
    <div className="mb-4">
      <dl className="mt-5 grid grid-cols-1 rounded-lg bg-white overflow-hidden shadow divide-y divide-gray-200 md:grid-cols-5 md:divide-y-0 md:divide-x">
        <div className="px-4 py-5 sm:p-4 bg-cyan-500 grid md:justify-items-end">
          <dd className="mt-1 flex items-baseline md:block lg:flex">
            <div className="flex items-baseline text-3xl font-extralight text-white">{total_gross}</div>
          </dd>
          <dt className="text-xl font-normal text-white font-extralight mt-1">Total Gross</dt>
        </div>
        <div className="px-4 py-5 sm:p-4 bg-purple-500 grid md:justify-items-end">
          <dd className="mt-1 flex items-baseline md:block lg:flex">
            <div className="flex items-baseline text-3xl font-extralight text-white">{total_net}</div>
          </dd>
          <dt className="text-xl font-normal text-white font-extralight mt-1">Total Net</dt>
        </div>
        <div className="px-4 py-5 sm:p-4 bg-green-500 grid md:justify-items-end">
          <dd className="mt-1 flex items-baseline md:block lg:flex">
            <div className="flex items-baseline text-3xl font-extralight text-white">{generalStats.num_clients}</div>
          </dd>
          <dt className="text-xl font-normal text-white font-extralight mt-1">Total Clients</dt>
        </div>
        <div className="px-4 py-5 sm:p-4 bg-orange-500 grid md:justify-items-end ">
          <dd className="mt-1 flex items-baseline md:block lg:flex">
            <div className="flex items-baseline text-3xl font-extralight text-white">{generalStats.active_clients}</div>
          </dd>
          <div className="relative group">
            <span className="absolute top-0 left-0 w-max px-2 py-1 rounded bg-white text-xs text-black whitespace-nowrap invisible group-hover:visible">
              This metric considers that a client is active if he<br></br> makes at least one purchase in every trimester
            </span>
            <dt className="text-xl font-normal text-white font-extralight mt-1 inline-flex">
              Active Clients
              <InformationCircleIcon className="h-5 w-5 pb-1 text-white cursor-pointer" />
            </dt>
          </div>
        </div>
        <div className="px-4 py-5 sm:p-4 bg-pink-500 grid md:justify-items-end">
          <dd className="mt-1 flex items-baseline md:block lg:flex">
            <div className="flex items-baseline text-3xl font-extralight text-white">{generalStats.avg_invoice}€</div>
          </dd>
          <dt className="text-xl font-normal text-white font-extralight mt-1">Average Sales</dt>
        </div>
      </dl>
    </div>
  );
};

export default Widgets;

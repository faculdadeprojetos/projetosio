import { MapContainer as Map, CircleMarker, TileLayer, Tooltip } from "react-leaflet";
import { CurrencyEuroIcon, HomeIcon, UsersIcon } from "@heroicons/react/outline";
import "leaflet/dist/leaflet.css";

const BubbleMap = ({ salesByCity }) => {
  let minLat = 36.172485;
  let maxLat = 42.427099;
  let minLong = -10.819847;
  let maxLong = -5.46191;
  let centerLat = (minLat + maxLat) / 2;
  let distanceLat = maxLat - minLat;
  let bufferLat = distanceLat * 0.05;
  let centerLong = (minLong + maxLong) / 2;
  let distanceLong = maxLong - minLong;
  let bufferLong = distanceLong * 0.05;

  return (
    <Map
      style={{ height: "320px", width: "100%", 'z-index': '0' }}
      zoom={6}
      center={[centerLat, centerLong]}
      bounds={[
        [minLat - bufferLat, minLong - bufferLong],
        [maxLat + bufferLat, maxLong + bufferLong],
      ]}
    >
      <TileLayer url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      {salesByCity.map((city, index) => {
        const radius = 8 * Math.log(city["total_gross"] / 1000);
        let fillColor;
        if (city["total_gross"] < 5000) {
          fillColor = "#5ad45a"; //green
        } else if (city["total_gross"] >= 5000 && city["total_gross"] < 20000) {
          fillColor = "#0d88e6"; //blue
        } else if (city["total_gross"] >= 20000 && city["total_gross"] < 50000) {
          fillColor = "#4421af"; //purle
        } else if (city["total_gross"] >= 50000 && city["total_gross"] < 1000000) {
          fillColor = "#7c1158"; //in the middle of purple and red
        } else if (city["total_gross"] >= 1000000) {
          fillColor = "#b30000"; //red
        }
        return (
          <CircleMarker
            key={`circle-${index}-${city["coordinates"][0]}`}
            center={[city["coordinates"][0], city["coordinates"][1]]}
            radius={radius}
            fillOpacity={0.5}
            stroke={false}
            fillColor={fillColor}
          >
            <Tooltip direction="right" offset={[-8, -2]} opacity={1}>
              <div className="p-1">
                <div className="group rounded-md flex items-center font-medium mb-1">
                  <HomeIcon className="mr-1 flex-shrink-0 h-4 w-4" aria-hidden="true" />
                  <span>{city["city"]}</span>
                </div>
                <div className="group rounded-md flex items-center font-medium">
                  <CurrencyEuroIcon className="mr-1 flex-shrink-0 h-4 w-4" aria-hidden="true" />
                  <span>{"Total gross: " + city["total_gross"] + "€"}</span>
                </div>
                <div className="group rounded-md flex items-center font-medium">
                  <span className="ml-5">{"Total net: " + city["total_net"]} €</span>
                </div>
                <div className="group rounded-md flex items-center font-medium">
                  <UsersIcon className="mr-1 flex-shrink-0 h-4 w-4" aria-hidden="true" />
                  <span>{"Number of sales: " + city["total_invoices"]}</span>
                </div>
              </div>
            </Tooltip>
          </CircleMarker>
        );
      })}
    </Map>
  );
};

export default BubbleMap;

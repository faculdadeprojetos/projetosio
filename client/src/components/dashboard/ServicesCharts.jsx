import { useState, useEffect } from "react";
import { Bar, Pie } from "react-chartjs-2";

const ProductCharts = ({ stats }) => {
  const [valueByService, setValueByService] = useState({});
  const [totalByService, setTotalByService] = useState({});

  const [isDataReady, setIsDataReady] = useState(false);

  useEffect(() => {
    const prepareDataForChart = (stats) => {
      const services = stats.totalByProduct.slice().filter((product) => product.product_type === "S");

      const labelsTotalServiceSold = services.map((item) => item.product_description);
      const totalSoldByService = services.map((item) => item.total_sold);

      const sortedServices = services.slice().sort((a, b) => parseFloat(b.total_billed) - parseFloat(a.total_billed));
      const labelsTotalServiceBilled = sortedServices.map((item) => (item.product_description.length > 25 ? item.product_description.slice(0, 25) + "..." : item.product_description)).slice(0, 5);
      const totalBilledByService = sortedServices.map((item) => item.total_billed).slice(0, 5);

      const valueByService = {
        labels: labelsTotalServiceSold,
        datasets: [
          {
            label: "Number of sales",
            data: totalSoldByService,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const totalByService = {
        labels: labelsTotalServiceBilled,
        datasets: [
          {
            label: "Total gross value",
            data: totalBilledByService,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };
      setTotalByService(totalByService);
      setValueByService(valueByService);
      setIsDataReady(true);
    };
    prepareDataForChart(stats);
  }, [stats]);

  return (
    <>
      {isDataReady && (
        <div className="flex mb-8 mt-8">
          <div className="w-2/3">
            <Bar
              data={totalByService}
              responsive="true"
              options={{
                indexAxis: "x",
                plugins: {
                  title: {
                    display: true,
                    align: "center",
                    text: "Top 5 services most selled",
                  },
                },
              }}
            />
          </div>
          <div className="w-1/3">
            <Pie
              data={valueByService}
              responsive="true"
              options={{
                indexAxis: "y",
                plugins: {
                  title: {
                    display: true,
                    align: "center",
                    text: "Number of sales by service",
                  },
                  legend: {
                    display: false,
                  },
                },
              }}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default ProductCharts;

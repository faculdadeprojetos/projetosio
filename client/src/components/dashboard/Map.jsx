import React, { useState, useEffect } from "react";
import { GoogleMap, MarkerF, InfoWindowF } from "@react-google-maps/api";
import { UserCircleIcon, HomeIcon } from "@heroicons/react/outline";

const Map = ({ clientsCoordinates }) => {
  const [selectedCustomer, setSelectedCustomer] = useState(null);
  const [map, setMap] = useState(null);

  const center = { lat: 39.3999, lng: -8.2245 }; // Portugal

  const handleLoad = (map) => {
    setMap(map);
  };

  const handleMarkerClick = (customer) => {
    setSelectedCustomer(customer);
  };

  const handleInfoWindowClose = () => {
    setSelectedCustomer(null);
  };

  useEffect(() => {
    if (selectedCustomer) {
      const marker = markers.find((m) => m.customer === selectedCustomer);
      const infoWindow = new window.google.maps.InfoWindow({
        content: `<div>${selectedCustomer.client_name}</div>`,
      });
      infoWindow.open(map, marker);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCustomer, map]);

  const markers = clientsCoordinates.map((client) => {
    const position = {
      lat: client.lat,
      lng: client.lng,
    };

    return <MarkerF key={client.client_id} position={position} onClick={() => handleMarkerClick(client)} customer={client} />;
  });

  return (
    <GoogleMap center={center} zoom={7} onLoad={handleLoad} mapContainerStyle={{ height: "50vh", width: "100%" }}>
      {markers}
      {selectedCustomer && (
        <InfoWindowF
          position={{
            lat: selectedCustomer.lat,
            lng: selectedCustomer.lng,
          }}
          onCloseClick={handleInfoWindowClose}
        >
          <div className="p-1">
            <div className="group rounded-md flex items-center font-medium mb-3">
              <UserCircleIcon className="mr-1 flex-shrink-0 h-4 w-4" aria-hidden="true" />
              {selectedCustomer.client_name}
            </div>
            <div className="group rounded-md flex items-center font-medium">
              <HomeIcon className="mr-1 flex-shrink-0 h-4 w-4" aria-hidden="true" />
              {selectedCustomer.address}, {selectedCustomer.city}
            </div>
          </div>
        </InfoWindowF>
      )}
    </GoogleMap>
  );
};

export default Map;

import { Map } from "..";
import { useState, useEffect } from "react";
import { Bar, Doughnut, PolarArea } from "react-chartjs-2";

const ClientCharts = ({ stats }) => {
  const [chartDataValues, setChartDataValues] = useState({});
  const [chartDataNumber, setChartDataNumber] = useState({});
  const [isDataReady, setIsDataReady] = useState(false);

  useEffect(() => {
    const prepareDataForChart = (stats) => {
      const grossValues = stats.bestClientsValue.map((item) => parseFloat(item.gross));
      const netValues = stats.bestClientsValue.map((item) => parseFloat(item.net));
      const labelsValues = stats.bestClientsValue.map((item) => item.client_name);

      const numberOfSales = stats.bestClientsNumber.map((item) => parseFloat(item.num_purchases));
      const labelsNumber = stats.bestClientsNumber.map((item) => item.client_name);

      const valuesData = {
        labels: labelsValues,
        datasets: [
          {
            label: "Gross value",
            data: grossValues,
            backgroundColor: "rgba(255, 206, 86, 0.5)",
          },
          {
            label: "Net value",
            data: netValues,
            backgroundColor: "rgba(75, 192, 192, 0.5)",
          },
        ],
      };

      const numberData = {
        labels: labelsNumber,
        datasets: [
          {
            label: "Number of sales",
            data: numberOfSales,
            backgroundcolor: ["rgba(255, 99, 132, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(255, 206, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(153, 102, 255, 0.2)"],
          },
        ],
      };
      setChartDataValues(valuesData);
      setChartDataNumber(numberData);
      setIsDataReady(true);
    };
    prepareDataForChart(stats);
  }, [stats]);

  return (
    <>
      {isDataReady && (
        <>
          <p className="text-center text-sm text-gray-500 font-bold">Clients by billing address</p>
          <Map clientsCoordinates={stats.coordinatesClients} />
          <div className="flex mb-8 mt-5 w-full">
            <div className="w-2/3">
              <Bar
                data={chartDataValues}
                responsive="true"
                options={{
                  indexAxis: "y",
                  plugins: {
                    title: {
                      display: true,
                      align: "center",
                      text: "Top 5 clients on the value of sales",
                    },
                  },
                }}
              />
            </div>
            <div className="w-1/3">
              <PolarArea
                responsive="true"
                options={{
                  indexAxis: "y",
                  plugins: {
                    title: {
                      display: true,
                      align: "center",
                      text: "Top 5 clients on the number of sales",
                    },
                  },
                }}
                data={chartDataNumber}
              />
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default ClientCharts;

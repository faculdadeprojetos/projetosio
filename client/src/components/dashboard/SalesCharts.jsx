import { Bar, Line, PolarArea } from "react-chartjs-2";
import { useState, useEffect } from "react";
import { BubbleMap } from "..";
import { formatDate, numberToDayWeek } from "../../helper/helper"

const SalesCharts = ({ stats }) => {
  const [chartMonth, setChartMonth] = useState({});
  const [chartMonthBoth, setChartMonthBoth] = useState({});
  const [chartMonthProducts, setChartMonthProducts] = useState({});
  const [chartMonthServices, setChartMonthServices] = useState({});

  const [numberOfSalesByMonth, setNumberOfSalesByMonth] = useState({});
  const [numberOfSalesByMonthBoth, setNumberOfSalesByMonthBoth] = useState({});
  const [numberOfSalesByMonthProducts, setNumberOfSalesByMonthProducts] = useState({});
  const [numberOfSalesByMonthServices, setNumberOfSalesByMonthServices] = useState({});

  const [salesByDayOfWeek, setSalesByDayOfWeek] = useState({});
  const [salesByDayOfWeekBoth, setSalesByDayOfWeekBoth] = useState({});
  const [salesByDayOfWeekProducts, setSalesByDayOfWeekProducts] = useState({});
  const [salesByDayOfWeekServices, setSalesByDayOfWeekServices] = useState({});

  const [byCityCoords, setByCityCoords] = useState({});
  const [byCityCoordsBoth, setByCityCoordsBoth] = useState({});
  const [byCityCoordsProducts, setByCityCoordsProducts] = useState({});
  const [byCityCoordsServices, setByCityCoordsServices] = useState({});

  const [numberOfSalesByCity, setNumberOfSalesByCity] = useState({});
  const [numberOfSalesByCityBoth, setNumberOfSalesByCityBoth] = useState({});
  const [numberOfSalesByCityProducts, setNumberOfSalesByCityProducts] = useState({});
  const [numberOfSalesByCityServices, setNumberOfSalesByCityServices] = useState({});

  const [valueOfSalesByCity, setValueOfSalesByCity] = useState({});
  const [valueOfSalesByCityProducts, setValueOfSalesByCityProducts] = useState({});
  const [valueOfSalesByCityServices, setValueOfSalesByCityServices] = useState({});
  const [valueOfSalesByCityBoth, setValueOfSalesByCityBoth] = useState({});


  const [isDataReady, setIsDataReady] = useState(false);
  const [salesTypeShow, setSalesTypeShow] = useState('both') //both, services or products

  useEffect(() => {
    const prepareDataForChart = (stats) => {

      const labelsSalesMonth = stats.salesByMonth.map((item) => formatDate(item.month + "-" + item.year));

      // MONTHLY EVOLUTION
      const netValuesMonth = stats.salesByMonth.map((item) => parseFloat(item.total_net));
      const grossValuesMonth = stats.salesByMonth.map((item) => parseFloat(item.total_gross));
      const diffValuesMonth = stats.salesByMonth.map((item) => parseFloat(item.diff));

      const netValuesMonthProducts = stats.salesByMonth.map((item) => parseFloat(item.total_net_products));
      const grossValuesMonthProducts = stats.salesByMonth.map((item) => parseFloat(item.total_gross_products));
      const diffValuesMonthProducts = stats.salesByMonth.map((item) => parseFloat(item.diff_products));

      const netValuesMonthServices = stats.salesByMonth.map((item) => parseFloat(item.total_net_services));
      const grossValuesMonthServices = stats.salesByMonth.map((item) => parseFloat(item.total_gross_services));
      const diffValuesMonthServices = stats.salesByMonth.map((item) => parseFloat(item.diff_services));

      //NUMBER OF SALES
      const numberSalesValuesMonthBoth = stats.salesByMonth.map((item) => item.total_invoices);
      const numberSalesValuesMonthProducts = stats.salesByMonth.map((item) => item.total_invoices_products);
      const numberSalesValuesMonthServices = stats.salesByMonth.map((item) => item.total_invoices_services);

      //WEEK DAYS
      const labelSalesDayWeekBoth = stats.salesByDayWeek.map((item) => numberToDayWeek(item.day_of_week));
      const valueSalesDayWeekBoth = stats.salesByDayWeek.map((item) => item.percentage);

      const labelSalesDayWeekProducts = stats.salesByDayWeekProducts.map((item) => numberToDayWeek(item.day_of_week));
      const valueSalesDayWeekProducts = stats.salesByDayWeekProducts.map((item) => item.percentage);

      const labelSalesDayWeekServices = stats.salesByDayWeekServices.map((item) => numberToDayWeek(item.day_of_week));
      const valueSalesDayWeekServices = stats.salesByDayWeekServices.map((item) => item.percentage);


      // BY CITY COORDS
      const sortedByNumberOfSalesBoth = stats.coordinatesCitiesBySales.slice().sort((a, b) => parseFloat(b.total_invoices) - parseFloat(a.total_invoices));
      const sortedByGrossValueBoth = stats.coordinatesCitiesBySales.slice().sort((a, b) => parseFloat(b.total_gross) - parseFloat(a.total_gross));
      const sortedByDiffBoth = stats.coordinatesCitiesBySales.slice().sort((a, b) => parseFloat(b.diff) - parseFloat(a.diff));
      
      const sortedByNumberOfSalesProducts = stats.coordinatesCitiesBySalesProducts.slice().sort((a, b) => parseFloat(b.total_invoices) - parseFloat(a.total_invoices));
      const sortedByGrossValueProducts = stats.coordinatesCitiesBySalesProducts.slice().sort((a, b) => parseFloat(b.total_gross) - parseFloat(a.total_gross));
      const sortedByDiffProducts = stats.coordinatesCitiesBySalesProducts.slice().sort((a, b) => parseFloat(b.diff) - parseFloat(a.diff));
      
      const sortedByNumberOfSalesServices = stats.coordinatesCitiesBySalesServices.slice().sort((a, b) => parseFloat(b.total_invoices) - parseFloat(a.total_invoices));
      const sortedByGrossValueServices = stats.coordinatesCitiesBySalesServices.slice().sort((a, b) => parseFloat(b.total_gross) - parseFloat(a.total_gross));
      const sortedByDiffServices = stats.coordinatesCitiesBySalesServices.slice().sort((a, b) => parseFloat(b.diff) - parseFloat(a.diff));



      // BY CITY TOTAL NUMBER OF SALES
      const numberOfSalesByCityChartBoth = sortedByNumberOfSalesBoth.map((item) => item.total_invoices).slice(0, 5);
      const labelOfSalesByCityChartBoth = sortedByNumberOfSalesBoth.map((item) => item.city).slice(0, 5);

      const numberOfSalesByCityChartProducts = sortedByNumberOfSalesProducts.map((item) => item.total_invoices).slice(0, 5);
      const labelOfSalesByCityChartProducts = sortedByNumberOfSalesProducts.map((item) => item.city).slice(0, 5);

      const numberOfSalesByCityChartServices = sortedByNumberOfSalesServices.map((item) => item.total_invoices).slice(0, 5);
      const labelOfSalesByCityChartServices = sortedByNumberOfSalesServices.map((item) => item.city).slice(0, 5);

      // BY CITY NET AND GROSS
      const numberOfGrossValueByCityChartBoth = sortedByGrossValueBoth.map((item) => item.total_gross).slice(0, 5);
      const numberOfNetValueByCityChartBoth = sortedByGrossValueBoth.map((item) => item.total_net).slice(0, 5);
      const numberOfDiffByCityChartBoth = sortedByDiffBoth.map((item) => item.diff).slice(0, 5);
      const labelOfGrossValueChartBoth = sortedByGrossValueBoth.map((item) => item.city).slice(0, 5);

      const numberOfGrossValueByCityChartProducts = sortedByGrossValueProducts.map((item) => item.total_gross).slice(0, 5);
      const numberOfNetValueByCityChartProducts = sortedByGrossValueProducts.map((item) => item.total_net).slice(0, 5);
      const numberOfDiffByCityChartProducts = sortedByDiffProducts.map((item) => item.diff).slice(0, 5);
      const labelOfGrossValueChartProducts = sortedByGrossValueProducts.map((item) => item.city).slice(0, 5);

      const numberOfGrossValueByCityServices = sortedByGrossValueServices.map((item) => item.total_gross).slice(0, 5);
      const numberOfNetValueByCityChartServices = sortedByGrossValueServices.map((item) => item.total_net).slice(0, 5);
      const numberOfDiffByCityChartServices = sortedByDiffServices.map((item) => item.diff).slice(0, 5);
      const labelOfGrossValueChartServices = sortedByGrossValueServices.map((item) => item.city).slice(0, 5);


      const numberOfSalesByMonthBoth = {
        labels: labelsSalesMonth,
        datasets: [
          {
            label: "Number of sales",
            data: numberSalesValuesMonthBoth,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };


      const numberOfSalesByMonthProducts = {
        labels: labelsSalesMonth,
        datasets: [
          {
            label: "Number of sales",
            data: numberSalesValuesMonthProducts,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };


      const numberOfSalesByMonthServices = {
        labels: labelsSalesMonth,
        datasets: [
          {
            label: "Number of sales",
            data: numberSalesValuesMonthServices,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const monthDataBoth = {
        labels: labelsSalesMonth,
        datasets: [
          {
            label: "Gross value",
            data: grossValuesMonth,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Net value",
            data: netValuesMonth,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Taxes",
            data: diffValuesMonth,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const monthDataProducts = {
        labels: labelsSalesMonth,
        datasets: [
          {
            label: "Gross value",
            data: grossValuesMonthProducts,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Net value",
            data: netValuesMonthProducts,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Taxes",
            data: diffValuesMonthProducts,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const monthDataServices = {
        labels: labelsSalesMonth,
        datasets: [
          {
            label: "Gross value",
            data: grossValuesMonthServices,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Net value",
            data: netValuesMonthServices,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Taxes",
            data: diffValuesMonthServices,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };


      const numberOfSalesByCityBoth = {
        labels: labelOfSalesByCityChartBoth,
        datasets: [
          {
            label: "Number of sales",
            data: numberOfSalesByCityChartBoth,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const numberOfSalesByCityProducts = {
        labels: labelOfSalesByCityChartProducts,
        datasets: [
          {
            label: "Number of sales",
            data: numberOfSalesByCityChartProducts,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const numberOfSalesByCityServices = {
        labels: labelOfSalesByCityChartServices,
        datasets: [
          {
            label: "Number of sales",
            data: numberOfSalesByCityChartServices,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };


      const valueOfSalesByCityBoth = {
        labels: labelOfGrossValueChartBoth,
        datasets: [
          {
            label: "Gross amount",
            data: numberOfGrossValueByCityChartBoth,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Net amount",
            data: numberOfNetValueByCityChartBoth,
            bordercolor: "rgb(0,255,255)",
            backgroundcolor: "rgba(255, 99, 233, 0.5)",
          },
          {
            label: "Taxes",
            data: numberOfDiffByCityChartBoth,
            bordercolor: "rgb(0,255,255)",
            backgroundcolor: "rgba(255, 99, 233, 0.5)",
          },
        ],
      };

      const valueOfSalesByCityProducts = {
        labels: labelOfGrossValueChartProducts,
        datasets: [
          {
            label: "Gross amount",
            data: numberOfGrossValueByCityChartProducts,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Net amount",
            data: numberOfNetValueByCityChartProducts,
            bordercolor: "rgb(0,255,255)",
            backgroundcolor: "rgba(255, 99, 233, 0.5)",
          },
          {
            label: "Taxes",
            data: numberOfDiffByCityChartProducts,
            bordercolor: "rgb(0,255,255)",
            backgroundcolor: "rgba(255, 99, 233, 0.5)",
          },
        ],
      };


      const valueOfSalesByCityServices = {
        labels: labelOfGrossValueChartServices,
        datasets: [
          {
            label: "Gross amount",
            data: numberOfGrossValueByCityServices,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
          {
            label: "Net amount",
            data: numberOfNetValueByCityChartServices,
            bordercolor: "rgb(0,255,255)",
            backgroundcolor: "rgba(255, 99, 233, 0.5)",
          },
          {
            label: "Taxes",
            data: numberOfDiffByCityChartServices,
            bordercolor: "rgb(0,255,255)",
            backgroundcolor: "rgba(255, 99, 233, 0.5)",
          },
        ],
      };


      const salesByDayOfWeekBoth = {
        labels: labelSalesDayWeekBoth,
        datasets: [
          {
            label: "Percentage of sales by day of the week",
            data: valueSalesDayWeekBoth,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const salesByDayOfWeekProducts = {
        labels: labelSalesDayWeekProducts,
        datasets: [
          {
            label: "Percentage of sales by day of the week",
            data: valueSalesDayWeekProducts,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const salesByDayOfWeekServices = {
        labels: labelSalesDayWeekServices,
        datasets: [
          {
            label: "Percentage of sales by day of the week",
            data: valueSalesDayWeekServices,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      setChartMonthProducts(monthDataProducts);
      setChartMonthBoth(monthDataBoth);
      setChartMonthServices(monthDataServices);
      setChartMonth(monthDataBoth)

      setNumberOfSalesByMonth(numberOfSalesByMonthBoth);
      setNumberOfSalesByMonthBoth(numberOfSalesByMonthBoth);
      setNumberOfSalesByMonthProducts(numberOfSalesByMonthProducts);
      setNumberOfSalesByMonthServices(numberOfSalesByMonthServices);


      setSalesByDayOfWeek(salesByDayOfWeekBoth)
      setSalesByDayOfWeekBoth(salesByDayOfWeekBoth)
      setSalesByDayOfWeekProducts(salesByDayOfWeekProducts)
      setSalesByDayOfWeekServices(salesByDayOfWeekServices)

      setByCityCoords(sortedByNumberOfSalesBoth)
      setByCityCoordsBoth(sortedByNumberOfSalesBoth)
      setByCityCoordsProducts(sortedByNumberOfSalesProducts)
      setByCityCoordsServices(sortedByNumberOfSalesServices)


      setNumberOfSalesByCity(numberOfSalesByCityBoth);
      setNumberOfSalesByCityBoth(numberOfSalesByCityBoth);
      setNumberOfSalesByCityProducts(numberOfSalesByCityProducts);
      setNumberOfSalesByCityServices(numberOfSalesByCityServices);

      setValueOfSalesByCityBoth(valueOfSalesByCityBoth);
      setValueOfSalesByCity(valueOfSalesByCityBoth);
      setValueOfSalesByCityProducts(valueOfSalesByCityProducts);
      setValueOfSalesByCityServices(valueOfSalesByCityServices);


      setIsDataReady(true);
    };
    prepareDataForChart(stats);
  }, [stats]);



  function changeSales(type) {
    setSalesTypeShow(type)

    //change Monthly evolution by the saves value
    if (type === 'services') {
      setChartMonth(chartMonthServices)
      setNumberOfSalesByMonth(numberOfSalesByMonthServices)
      setSalesByDayOfWeek(salesByDayOfWeekBoth)
      setByCityCoords(byCityCoordsServices)
      setNumberOfSalesByCity(numberOfSalesByCityServices)
      setValueOfSalesByCity(valueOfSalesByCityServices)
    } else if (type === 'products') {
      setChartMonth(chartMonthProducts)
      setNumberOfSalesByMonth(numberOfSalesByMonthProducts)
      setSalesByDayOfWeek(salesByDayOfWeekProducts)
      setByCityCoords(byCityCoordsProducts)
      setNumberOfSalesByCity(numberOfSalesByCityProducts)
      setValueOfSalesByCity(valueOfSalesByCityProducts)
    } else {
      setChartMonth(chartMonthBoth)
      setNumberOfSalesByMonth(numberOfSalesByMonthBoth)
      setSalesByDayOfWeek(salesByDayOfWeekBoth)
      setByCityCoords(byCityCoords)
      setNumberOfSalesByCity(numberOfSalesByCityBoth)
      setValueOfSalesByCity(valueOfSalesByCityBoth)
    }

  }

  return (
    <>
      {isDataReady && (
        <div className="mb-6">
          <div className="w-full flex justify-end my-5">
            <button className={` ${salesTypeShow === 'products' ? 'bg-blue-900' : 'bg-blue-400'}  text-white font-bold py-2 px-4 rounded`}
              onClick={() => { changeSales('products') }}>
              Products
            </button>
            <button className={` ${salesTypeShow === 'services' ? 'bg-blue-900' : 'bg-blue-400'} mx-10  text-white font-bold py-2 px-4 rounded`}
              onClick={() => { changeSales('services') }}>
              Services
            </button>
            <button className={` ${salesTypeShow === 'both' ? 'bg-blue-900' : 'bg-blue-400'}  text-white font-bold py-2 px-4 rounded`}
              onClick={() => { changeSales('both') }}>
              Both
            </button>
          </div>
          <div className="w-full flex justify-between">
            <div className="w-full">
              <Line
                responsive="true"
                data={chartMonth}
                options={{
                  plugins: {
                    title: {
                      display: true,
                      align: "center",
                      text: "Monthly evolution by the sales amount",
                      font: {
                        size: 16
                      }
                    },
                  },
                }}
              />
            </div>
            <div className="w-full ml-10">
              <Line
                responsive="true"
                data={numberOfSalesByMonth}
                options={{
                  plugins: {
                    title: {
                      display: true,
                      align: "center",
                      text: "Monthly evolution by number of sales",
                      font: {
                        size: 16
                      }
                    },
                  },
                }}
              />
            </div>
          </div>
          <div className="w-full flex flex-between">
            <div className="w-1/2">
              <h1 className="text-center text-lg font-bold text-gray-500 mb-4">Sales by city</h1>
              <BubbleMap salesByCity={byCityCoords} className="z-0" />
            </div>
            <div className="ml-10 mt-auto w-1/2">
              <Bar
                responsive="true"
                data={salesByDayOfWeek}
                options={{
                  indexAxis: "x",
                  elements: {
                    bar: {
                      borderWidth: 2,
                      barPercentage: 0.2
                    }
                  },
                  plugins: {
                    title: {
                      display: true,
                      align: "center",
                      text: "Distribution of sales by the days of the week",
                      font: {
                        size: 16
                      }
                    },
                  },
                }}
              />
            </div>
          </div>
          <div className="flex mb-8 mt-7">
            <div className="w-1/3">
              <PolarArea
                data={numberOfSalesByCity}
                responsive="true"
                options={{
                  indexAxis: "y",
                  plugins: {
                    title: {
                      display: true,
                      align: "center",
                      text: "Top 5 cities on number of sales",
                      font: {
                        size: 16
                      }
                    },
                  },
                }}
              />
            </div>
            <div className="w-2/3 ml-10">
              <Bar
                data={valueOfSalesByCity}
                responsive="true"
                options={{
                  indexAxis: "x",
                  plugins: {
                    title: {
                      display: true,
                      align: "center",
                      text: "Top 5 cities on the value of sales",
                      font: {
                        size: 16
                      }
                    },
                  },
                }}
              />
            </div>
          </div>
        </div >
      )}
    </>
  );
};

export default SalesCharts;

import { useState, useEffect } from "react";
import { Bar, Pie } from "react-chartjs-2";

const FamilyGroupCharts = ({ stats }) => {
  const [valueByFamilyGroup, setValueByFamilyGroup] = useState({});
  const [totalByFamilyGroup, setTotalByFamilyGroup] = useState({});
  const [isDataReady, setIsDataReady] = useState(false);

  useEffect(() => {
    const prepareDataForChart = (stats) => {
      const labelsFamilyGroups = stats.totalByFamilyProduct.map((item) => item.product_group);
      const totalSoldByFamily = stats.totalByFamilyProduct.map((item) => item.total_sold);

      const sortedFamilyGroups = stats.totalByFamilyProduct.slice().sort((a, b) => parseFloat(b.total_billed) - parseFloat(a.total_billed));
      const labelsTotalBilledByFamily = sortedFamilyGroups.map((item) => (item.product_group.length > 25 ? item.product_group.slice(0, 25) + "..." : item.product_group)).slice(0, 5);
      const totalBilledByFamily = sortedFamilyGroups.map((item) => item.total_billed).slice(0, 5);

      const valueByFamily = {
        labels: labelsTotalBilledByFamily,
        datasets: [
          {
            label: "Total gross value",
            data: totalBilledByFamily,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      const totalByFamily = {
        labels: labelsFamilyGroups,
        datasets: [
          {
            label: "Number of sales",
            data: totalSoldByFamily,
            bordercolor: "rgb(255, 99, 132)",
            backgroundcolor: "rgba(53, 162, 235, 0.5)",
          },
        ],
      };

      setValueByFamilyGroup(valueByFamily);
      setTotalByFamilyGroup(totalByFamily);
      setIsDataReady(true);
    };
    prepareDataForChart(stats);
  }, [stats]);

  return (
    <>
      {isDataReady && (
        <div className="flex mb-8 mt-8">
          <div className="w-1/3">
            <Pie
              data={totalByFamilyGroup}
              responsive="true"
              options={{
                indexAxis: "y",
                plugins: {
                  title: {
                    display: true,
                    align: "center",
                    text: "Number of sales by family group",
                  },
                  legend: {
                    display: false,
                  },
                },
              }}
            />
          </div>
          <div className="w-2/3">
            <Bar
              data={valueByFamilyGroup}
              responsive="true"
              options={{
                indexAxis: "x",
                plugins: {
                  title: {
                    display: true,
                    align: "center",
                    text: "Top 5 family groups most selled",
                  },
                },
              }}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default FamilyGroupCharts;

import Modal from "./Modal";
import Map from "./dashboard/Map";
import BubbleMap from "./dashboard/BubbleMap";
import Widgets from "./dashboard/Widgets";
import SalesCharts from "./dashboard/SalesCharts";
import ClientCharts from "./dashboard/ClientCharts";
import ProductCharts from "./dashboard/ProductCharts";
import ServicesCharts from "./dashboard/ServicesCharts";
import FamilyGroupsCharts from "./dashboard/FamilyGroupsCharts";

export { Modal, Map, BubbleMap, Widgets, SalesCharts, ClientCharts, ProductCharts, ServicesCharts, FamilyGroupsCharts };

class Product {
  constructor(obj) {
    let tempObj = {
      ProductType: obj.ProductType[0],
      ProductCode: obj.ProductCode[0],
      ProductDescription: obj.ProductDescription[0],
      ProductNumberCode: obj.ProductNumberCode[0],
      ProductGroup: obj.ProductGroup[0],
    };
    this.checkProperties(tempObj);

    this.ProductType = tempObj.ProductType;
    this.ProductCode = tempObj.ProductCode;
    this.ProductDescription = tempObj.ProductDescription;
    this.ProductNumberCode = tempObj.ProductNumberCode;
    this.ProductGroup = tempObj.ProductGroup;
  }

  checkProperties(properties) {
    const expectedTypes = {
      ProductType: "string",
      ProductCode: "string",
      ProductDescription: "string",
      ProductNumberCode: "string",
      ProductGroup: "string",
    };
    for (const [key, type] of Object.entries(expectedTypes)) {
      if (!properties.hasOwnProperty(key)) {
        console.log(`Missing property: ${key}`);
        throw new Error(`Missing property: ${key}`);
      }
      if (typeof properties[key] !== type) {
        console.log(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
        throw new Error(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
      }
      if (properties[key] === null) {
        console.log(`Property ${key} should not be null`);
        throw new Error(`Property ${key} should not be null`);
      }
    }
  }
}

module.exports = Product;

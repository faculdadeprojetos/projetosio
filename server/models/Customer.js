class Customer {
  constructor(obj) {
    let tempObj = {
      CustomerID: parseInt(obj.CustomerID[0]),
      CompanyName: obj.CompanyName[0],
    };
    this.checkProperties(tempObj);

    this.CustomerID = tempObj.CustomerID;
    this.CompanyName = tempObj.CompanyName;
    this.BillingAddress = [];
  }

  checkProperties(properties) {
    const expectedTypes = {
      CustomerID: "number",
      CompanyName: "string",
    };
    for (const [key, type] of Object.entries(expectedTypes)) {
      if (!properties.hasOwnProperty(key)) {
        console.log(`Missing property: ${key}`);
        throw new Error(`Missing property: ${key}`);
      }
      if (typeof properties[key] !== type) {
        console.log(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
        throw new Error(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
      }
      if (properties[key] === null) {
        console.log(`Property ${key} should not be null`);
        throw new Error(`Property ${key} should not be null`);
      }
    }
  }

  addBillingAddress(billingAddress) {
    this.BillingAddress.push(billingAddress)
  }
}

module.exports = Customer;

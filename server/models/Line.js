class Line {
  constructor(obj, invoiceNo) {
    let tempObj = {
      LineNumber: parseInt(obj.LineNumber[0]),
      ProductCode: obj.ProductCode[0],
      Quantity: parseInt(obj.Quantity[0]),
      UnitOfMeasure: obj.UnitOfMeasure[0],
      UnitPrice: parseFloat(obj.UnitPrice[0]),
      TaxPointDate: obj.TaxPointDate[0],
      ProductDescription: obj.ProductDescription[0],
      Description: obj.Description[0],
    };

    if (obj.hasOwnProperty("DebitAmount")) {
      try {
        this.Amount = parseFloat(obj.DebitAmount[0]);
      } catch {
        throw new Error(`Invalid type for Amount: expected Number, but got ${typeof obj.DebitAmount[0]}`);
      }
    } else {
      try {
        this.Amount = parseFloat(obj.CreditAmount[0]);
      } catch {
        throw new Error(`Invalid type for Amount: expected Number, but got ${typeof obj.CreditAmount[0]}`);
      }
    }

    this.checkProperties(tempObj);

    //if the types are all right then create the instance of the object
    this.LineNumber = tempObj.LineNumber;
    this.ProductCode = tempObj.ProductCode;
    this.Quantity = tempObj.Quantity;
    this.UnitOfMeasure = tempObj.UnitOfMeasure;
    this.UnitPrice = tempObj.UnitPrice;
    this.TaxPointDate = tempObj.TaxPointDate;
    this.ProductDescription = tempObj.ProductDescription;
    this.Description = tempObj.Description;
    this.TaxIdTable = -1;
    this.InvoiceNo = invoiceNo;
    this.Tax = {
      TaxType: obj.Tax[0].TaxType[0],
      TaxCountryRegion: obj.Tax[0].TaxCountryRegion[0],
      TaxCode: obj.Tax[0].TaxCode[0],
      TaxPercentage: parseInt(obj.Tax[0].TaxPercentage[0]),
    };
  }

  checkProperties(properties) {
    const expectedTypes = {
      LineNumber: "number",
      ProductCode: "string",
      Quantity: "number",
      UnitOfMeasure: "string",
      UnitPrice: "number",
      TaxPointDate: "string",
      ProductDescription: "string",
      Description: "string",
    };
    for (const [key, type] of Object.entries(expectedTypes)) {
      if (!properties.hasOwnProperty(key)) {
        throw new Error(`Missing property: ${key}`);
      }
      if (typeof properties[key] !== type) {
        throw new Error(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
      }
      if (properties[key] === null) {
        throw new Error(`Property ${key} should not be null`);
      }
    }
  }
}

module.exports = Line;

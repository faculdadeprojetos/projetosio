class GeneralInfo {
    constructor(obj) {
        let tempObj = {
            FiscalYear: obj.FiscalYear,
            StartDate: obj.StartDate,
            EndDate: obj.EndDate,
            CurrencyCode: obj.CurrencyCode,
        }
        this.checkProperties(tempObj)

        this.FiscalYear = tempObj.FiscalYear
        this.StartDate = tempObj.StartDate
        this.EndDate = tempObj.EndDate
        this.CurrencyCode = tempObj.CurrencyCode
    }

    checkProperties(properties) {
        const expectedTypes = {
            FiscalYear: "number",
            StartDate: "string",
            EndDate: "string",
            CurrencyCode: "string",
        }
        for (const [key, type] of Object.entries(expectedTypes)) {
            if (!properties.hasOwnProperty(key)) {
                console.log(`Missing property: ${key}`)
                throw new Error(`Missing property: ${key}`)
            }
            if (typeof properties[key] !== type) {
                console.log(
                    `Invalid type for ${key}: expected ${type}, but got ${typeof properties[
                        key
                    ]}`
                )
                throw new Error(
                    `Invalid type for ${key}: expected ${type}, but got ${typeof properties[
                        key
                    ]}`
                )
            }
            if (properties[key] === null) {
                console.log(`Property ${key} should not be null`)
                throw new Error(`Property ${key} should not be null`)
            }
        }
    }
}

module.exports = GeneralInfo

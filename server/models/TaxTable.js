class TaxTable {
  constructor(obj) {
    let tempObj = {
      TaxType: obj.TaxType[0],
      TaxCountryRegion: obj.TaxCountryRegion[0],
      TaxCode: obj.TaxCode[0],
      Description: obj.Description[0],
      TaxPercentage: parseInt(obj.TaxPercentage[0]),
    };
    this.checkProperties(tempObj);

    this.Id = -1;
    this.TaxType = tempObj.TaxType;
    this.TaxCountryRegion = tempObj.TaxCountryRegion;
    this.TaxCode = tempObj.TaxCode;
    this.Description = tempObj.Description;
    this.TaxPercentage = tempObj.TaxPercentage;
  }

  checkProperties(properties) {
    const expectedTypes = {
      TaxType: "string",
      TaxCountryRegion: "string",
      TaxCode: "string",
      Description: "string",
      TaxPercentage: "number",
    };
    for (const [key, type] of Object.entries(expectedTypes)) {
      if (!properties.hasOwnProperty(key)) {
        console.log(`Missing property: ${key}`);
        throw new Error(`Missing property: ${key}`);
      }
      if (typeof properties[key] !== type) {
        console.log(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
        throw new Error(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
      }
      if (properties[key] === null) {
        console.log(`Property ${key} should not be null`);
        throw new Error(`Property ${key} should not be null`);
      }
    }
  }
}

module.exports = TaxTable;

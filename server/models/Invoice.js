class Invoice {
  constructor(obj) {
    let tempObj = {
      InvoiceNo: obj.InvoiceNo[0],
      ATCUD: obj.ATCUD[0],
      InvoiceStatus: obj.DocumentStatus[0].InvoiceStatus[0],
      SourceID: obj.DocumentStatus[0].SourceID[0],
      InvoiceStatusDate: obj.DocumentStatus[0].InvoiceStatusDate[0],
      SourceBilling: obj.DocumentStatus[0].SourceBilling[0],
      Hash: obj.Hash[0],
      HashControl: obj.HashControl[0],
      Period: parseInt(obj.Period[0]),
      InvoiceDate: obj.InvoiceDate[0],
      InvoiceType: obj.InvoiceType[0],
      CustomerID: parseInt(obj.CustomerID[0]),
      TaxPayable: parseFloat(obj.DocumentTotals[0].TaxPayable),
      NetTotal: parseFloat(obj.DocumentTotals[0].NetTotal),
      GrossTotal: parseFloat(obj.DocumentTotals[0].GrossTotal),
    };

    this.checkProperties(tempObj);

    this.InvoiceNo = tempObj.InvoiceNo;
    this.ATCUD = tempObj.ATCUD;
    this.InvoiceStatus = tempObj.InvoiceStatus;
    this.SourceID = tempObj.SourceID;
    this.InvoiceStatusDate = tempObj.InvoiceStatusDate;
    this.SourceBilling = tempObj.SourceBilling;
    this.Hash = tempObj.Hash;
    this.HashControl = tempObj.HashControl;
    this.Period = tempObj.Period;
    this.InvoiceDate = tempObj.InvoiceDate;
    this.InvoiceType = tempObj.InvoiceType;
    this.CustomerID = tempObj.CustomerID;
    this.TaxPayable = tempObj.TaxPayable;
    this.NetTotal = tempObj.NetTotal;
    this.GrossTotal = tempObj.GrossTotal;
    this.Lines = [];
  }

  checkProperties(properties) {
    const expectedTypes = {
      InvoiceNo: "string",
      ATCUD: "string",
      InvoiceStatus: "string",
      SourceID: "string",
      InvoiceStatusDate: "string",
      SourceBilling: "string",
      Hash: "string",
      HashControl: "string",
      Period: "number",
      InvoiceDate: "string",
      InvoiceType: "string",
      CustomerID: "number",
      TaxPayable: "number",
      NetTotal: "number",
      GrossTotal: "number",
    };
    for (const [key, type] of Object.entries(expectedTypes)) {
      if (!properties.hasOwnProperty(key)) {
        console.log(`Missing property: ${key}`);
        throw new Error(`Missing property: ${key}`);
      }
      if (typeof properties[key] !== type) {
        console.log(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
        throw new Error(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
      }
      if (properties[key] === null) {
        console.log(`Property ${key} should not be null`);
        throw new Error(`Property ${key} should not be null`);
      }
    }
  }

  addLine(line) {
    this.Lines.push(line);
  }
}

module.exports = Invoice;

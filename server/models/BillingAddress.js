class BillingAddress {
  constructor(obj) {
    let tempObj = {
      AddressDetail: obj.AddressDetail[0],
      City: obj.City[0],
      PostalCode: obj.PostalCode[0].slice(0,8),
      Country: obj.Country[0],
    };
    this.checkProperties(tempObj);

    //if the types are all right then create the instance of the object
    this.AddressDetail = tempObj.AddressDetail;
    this.City = tempObj.City;
    this.PostalCode = tempObj.PostalCode;
    this.Country = tempObj.Country;
  }

  checkProperties(properties) {
    const expectedTypes = {
      AddressDetail: "string",
      City: "string",
      PostalCode: "string",
      Country: "string",
    };
    for (const [key, type] of Object.entries(expectedTypes)) {
      if (!properties.hasOwnProperty(key)) {
        throw new Error(`Missing property: ${key}`);
      }
      if (typeof properties[key] !== type) {
        throw new Error(`Invalid type for ${key}: expected ${type}, but got ${typeof properties[key]}`);
      }
      if (properties[key] === null) {
        throw new Error(`Property ${key} should not be null`);
      }
    }
  }
}

module.exports = BillingAddress;

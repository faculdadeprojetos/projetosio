var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var jwt = require("jsonwebtoken");

var indexRouter = require("./routes/index");
var productsRouter = require("./routes/product");
var clientsRouter = require("./routes/client");
var invoicesRouter = require("./routes/invoice");
var taxesRouter = require("./routes/tax");
var authRouter = require("./routes/auth");

require("dotenv").config();

//database configuration/interaction file
var db = require("./helper/Db_connection");
try {
  db.connect();
} catch {
  console.log("Error trying to connect to the mysql database");
}

const cors = require("cors");

var app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// auth middleware
const authMiddleware = (req, res, next) => {
  if (req.url === "/auth/register" || req.url === "/auth/login") {
    next();
  } else {
    const token = req.header("Authorization");
    console.log(token)
    if (!token) {
      return res.status(401).json({ message: "No token, authorization denied" });
    }

    try {
      const decoded = jwt.verify(token, process.env.SECRET_KEY);
      req.user = decoded.user;
      next();
    } catch (err) {
      res.status(401).json({ message: "Invalid token" });
    }
  }
};

app.use("/", authMiddleware, indexRouter);
app.use("/products", authMiddleware, productsRouter);
app.use("/clients", authMiddleware, clientsRouter);
app.use("/invoices", authMiddleware, invoicesRouter);
app.use("/taxes", authMiddleware, taxesRouter);
app.use("/auth", authRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

app.use((req, res, next) => {
  //res.header("Access-Control-Allow-Origin", "*")
  res.header(
    "Access-Control-Allow-Origin",
    "http://localhost:3000",
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH");
  next();
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;

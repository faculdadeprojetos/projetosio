var express = require("express")
var router = express.Router()
var indexController = require("../controllers/IndexController")

/* POST upload .xml file */
router.post("/upload-xml", function (req, res, next) {
    indexController.uploadXML(req, res, next)
})

router.get("/parse", function (req, res, next) {
    indexController.parse(req, res, next)
})

router.get("/stats", function (req, res, next) {
    indexController.stats(req, res, next)
})

router.get("/delete", function (req, res, next) {
    indexController.delete(req, res, next)
})

module.exports = router

var express = require("express");
var router = express.Router();
var invoiceController = require("../controllers/InvoiceController");

/* GET clients listing. */
router.get("/", function (req, res, next) {
  invoiceController.list(req, res);
});

/* GET invoice lines. */
router.get("/invoiceLines", function (req, res, next) {
  if (!req.query.invoiceNo) {
    console.log("here");
    return res.send({ error: "Server Error" });
  }
  invoiceController.invoiceLines(req, res, next);
});

module.exports = router;

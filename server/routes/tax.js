var express = require("express");
var router = express.Router();
var taxesController = require("../controllers/TaxController");

/* GET taxes listing. */
router.get("/", function (req, res, next) {
  taxesController.list(req, res);
});

module.exports = router;

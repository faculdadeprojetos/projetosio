var express = require("express")
var router = express.Router()
var clientController = require("../controllers/ClientController")

/* GET clients listing. */
router.get("/", function (req, res, next) {
    clientController.list(req, res)
})

/* GET clients listing. */
router.get("/customerInfo/:customerId", function (req, res, next) {
    if (!req.params.customerId) {
        return res.send()
    }
    clientController.detailedInfo(req.params.customerId, req, res, next)
})

module.exports = router

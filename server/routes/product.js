var express = require("express");
var router = express.Router();
var productController = require("../controllers/ProductController");

/* GET products listing. */
router.get("/", function (req, res, next) {
  productController.list(req, res);
});

/* POST product insert. */
router.post("/insert", function (req, res, next) {
  productController.insert(req, res);
});

/* GET product info. */
router.get("/productInfo/:productId", function (req, res, next) {
  if (!req.params.productId) {
      return res.send()
  }
  productController.detailedInfo(req.params.productId, req, res, next)
})

module.exports = router;

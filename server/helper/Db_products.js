const db = require("./Db_connection.js")

db.insertMutipleProducts = async function (data) {
    const placeholders = data.map(() => "(?, ?, ?, ?, ?)").join(", ")

    // array of values to insert
    const values = data.reduce((acc, curr) => {
        acc.push(
            curr.ProductType,
            curr.ProductCode,
            curr.ProductDescription,
            curr.ProductNumberCode,
            curr.ProductGroup
        )
        return acc
    }, [])

    const sql = `INSERT INTO product (product_type, product_code, product_description, product_number_code, product_group) VALUES ${placeholders}`

    connection
        .execute(sql, values)
        .then((results) => {
            console.log(
                "Products: " + results[0].affectedRows + " rows inserted"
            )
        })
        .catch((error) => {
            console.error(error)
        })
}

db.selectProducts = async function () {
    const [rows] = await connection.query("SELECT * FROM product;")
    return await rows
}

db.totalInSales = async function (id) {
    const [rows] = await connection.query(
        `SELECT SUM(unit_price) AS total_in_sales
        FROM sio.invoice_line
        WHERE product_code = ${id};`
    );
    return await rows;
};

db.totalSales = async function (id) {
    const [rows] = await connection.query(
        `SELECT SUM(quantity) AS total_sales
        FROM sio.invoice_line
        WHERE product_code = ${id};`
    );
    return await rows;
};

//Local with more sales
db.bestLocal = async function (id) {
    const [rows] = await connection.query(
        `SELECT b.city, b.country, SUM(il.unit_price) AS best_local_total
        FROM sio.billing_address AS b
        JOIN sio.billing_address_customer AS bac ON bac.billing_id = b.billing_id
        JOIN sio.invoice AS i ON i.customer_id = bac.client_id
        JOIN sio.invoice_line AS il ON il.invoice_id = i.invoice_no
        WHERE il.product_code = ${id}
        GROUP BY b.city, b.country
        ORDER BY SUM(il.unit_price) DESC
        LIMIT 1;`
    );
    return await rows;
};

db.productSales = async function (id) {
    const [rows] = await connection.query(
        `SELECT invoice_date, invoice_no, invoice_type, customer_id, gross_total
        FROM sio.invoice
        WHERE invoice_no IN (
          SELECT invoice_id
          FROM sio.invoice_line
          WHERE product_code = ${id}) 
        ORDER BY invoice_date;`
    );
    return await rows;
};

module.exports = db

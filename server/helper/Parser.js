const BillingAddress = require("../models/BillingAddress")
const Customer = require("../models/Customer")
const GeneralInfo = require("../models/GeneralInfo")
const Product = require("../models/Product")
const TaxTable = require("../models/TaxTable")
const Invoice = require("../models/Invoice")
const Line = require("../models/Line")

var parser = {}

//parse the general information
parser.parseGeneralInfo = function (generalInfo) {
    let generalInfoObj = {}

    try {
      var obj = {}
      obj.FiscalYear = Number(generalInfo.FiscalYear[0])
      obj.StartDate = generalInfo.StartDate[0]
      obj.EndDate = generalInfo.EndDate[0]
      obj.CurrencyCode = generalInfo.CurrencyCode[0]
        const generalInfoTmp = new GeneralInfo(obj)
        generalInfoObj = generalInfoTmp
    } catch {
        console.error(
            "Error trying to parse General Info " + generalInfo
        )
    }
    return generalInfoObj
}

//parse customers and all his billing address information
parser.parseCustomers = function (customers) {
    let customerTemp = []

    for (let i = 0; i < customers.length; i++) {
        try {
            const customer = new Customer(customers[i])

            if (customers[i].BillingAddress != null) {
                for (let j = 0; j < customers[i].BillingAddress.length; j++) {
                    try {
                        const billingAddressInstance = new BillingAddress(
                            customers[i].BillingAddress[j]
                        )
                        customer.addBillingAddress(billingAddressInstance)
                    } catch {
                        console.error(
                            "Error trying to parse Billing Address of the Customer " +
                                customer.CompanyName
                        )
                    }
                }
            }

            customerTemp.push(customer)
        } catch {
            console.error("Error trying to parse Customer")
        }
    }

    return customerTemp
}

//parse products
parser.parseProducts = function (products) {
    let productsTemp = []

    for (let i = 0; i < products.length; i++) {
        try {
            const product = new Product(products[i])

            productsTemp.push(product)
        } catch {
            console.error("Error trying to parse Product")
        }
    }

    return productsTemp
}

//parse tax table with all the existent tax types
parser.parseTaxTable = function (taxTable) {
    let taxTableTemp = []

    for (let i = 0; i < taxTable.length; i++) {
        try {
            const tax = new TaxTable(taxTable[i])

            taxTableTemp.push(tax)
        } catch {
            console.error("Error trying to parse Tax")
        }
    }

    return taxTableTemp
}

//parse invoices and all his lines
parser.parseInvoices = function (invoices) {
    let invoicesTemp = []
    let linesTemp = []

    for (let i = 0; i < invoices.length; i++) {
        try {
            const invoice = new Invoice(invoices[i])

            for (let j = 0; j < invoices[i].Line.length; j++) {
                try {
                    const line = new Line(
                        invoices[i].Line[j],
                        invoices[i].InvoiceNo[0]
                    )

                    linesTemp.push(line)
                    //invoice.addLine(line);
                } catch {
                    console.error(
                        "Error trying to parse Line of the Invoice " +
                            invoice.InvoiceNo
                    )
                }
            }
            invoicesTemp.push(invoice)
        } catch {
            console.error("Error trying to parse Invoice")
        }
    }

    return [invoicesTemp, linesTemp]
}

module.exports = parser

const db = require("./Db_connection.js")

//top best clients by value
db.getGeneralInfo = async function () {
    const [rows] = await connection.query(
        `SELECT *
        FROM sio.general_info;`
    )
    return await rows[0]
}

//top best clients by value
db.topBestClientsByValue = async function (startDate, endDate) {
    const [rows] = await connection.query(
        `SELECT c.client_name, SUM(i.gross_total) AS gross, SUM(i.net_total) AS net
          FROM sio.invoice i
          JOIN sio.customer c ON i.customer_id = c.client_id
          WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
          GROUP BY c.client_id, c.client_name
          ORDER BY gross DESC
          LIMIT 5;`
    )
    return await rows
}

//top best clients by number of purchases
db.topBestClientsByNumberPurchases = async function (startDate, endDate) {
    const [rows] = await connection.query(
        `SELECT c.client_name, COUNT(*) as num_purchases
          FROM sio.customer c
          JOIN sio.invoice i ON c.client_id = i.customer_id
          WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
          GROUP BY c.client_id
          ORDER BY num_purchases DESC
          LIMIT 5;`
    )
    return await rows
}

//get the billing address of every client (only postal code)
db.addressOfClients = async function () {
    const [rows] = await connection.query(
        `SELECT c.client_id, c.client_name,ba.postal_code, ba.address_detail as address, ba.city, ba.country
          FROM sio.customer as c,
               sio.billing_address_customer as bac,
               sio.billing_address as ba
          WHERE bac.billing_id = ba.billing_id 
            and c.client_id = bac.client_id`
    )
    return await rows
}

//total gross value and total net value
db.getTotalGrossAndNet = async function (startDate, endDate) {
    const [rows] = await connection.query(`
        SELECT SUM(net_total) AS total_net, 
            SUM(gross_total) AS total_gross 
        FROM invoice as i
        WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
        `)
    return await rows
}

//average of all invoices and the number of clients (that purchased something)
db.getAvgInvoiceAndCountClients = async function (startDate, endDate) {
    const [rows] = await connection.query(`
        SELECT COUNT(DISTINCT customer_id) AS num_clients, 
            AVG(gross_total) AS avg_invoice 
        FROM invoice as i
        WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'`)
    return await rows
}

//total gross and net billed by month
db.getSalesByMonth = async function (startDate, endDate) {
    const [rows] = await connection.query(`
    SELECT YEAR(invoice_date)                                                             AS year,
    MONTH(invoice_date)                                                            AS month,
    SUM(CASE WHEN p.product_type = 'P' THEN gross_total ELSE 0 END)                AS total_gross_products,
    SUM(CASE WHEN p.product_type = 'P' THEN net_total ELSE 0 END)                  AS total_net_products,
    SUM(CASE WHEN p.product_type = 'P' THEN gross_total - net_total ELSE 0 END)    AS diff_products,

    SUM(CASE WHEN p.product_type = 'S' THEN gross_total ELSE 0 END)                AS total_gross_services,
    SUM(CASE WHEN p.product_type = 'S' THEN net_total ELSE 0 END)                  AS total_net_services,
    SUM(CASE WHEN p.product_type = 'S' THEN gross_total - net_total ELSE 0 END)    AS diff_services,

    SUM(gross_total)                                                               AS total_gross,
    SUM(net_total)                                                                 AS total_net,
    SUM(gross_total - net_total)                                                   AS diff,
    COUNT(DISTINCT CASE WHEN p.product_type = 'S' THEN i.invoice_no ELSE NULL END) AS total_invoices_services,
    COUNT(DISTINCT CASE WHEN p.product_type = 'P' THEN i.invoice_no ELSE NULL END) AS total_invoices_products,
    COUNT(DISTINCT i.invoice_no)                                                   AS total_invoices

FROM sio.invoice AS i
      JOIN sio.invoice_line AS il ON i.invoice_no = il.invoice_id
      JOIN sio.product AS p ON il.product_code = p.product_code
WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
GROUP BY YEAR(invoice_date), MONTH(invoice_date)
ORDER BY YEAR(invoice_date), MONTH(invoice_date);
`)
    return await rows
}

//total gross and net billed by city
db.getSumSalesByCity = async function (startDate, endDate) {
    const [rows] = await connection.query(`
        SELECT ba.city, 
            COUNT(i.invoice_no) AS total_invoices, 
            SUM(i.gross_total) AS total_gross, 
            SUM(i.net_total) AS total_net,
            SUM(i.gross_total - i.net_total) AS diff
        FROM customer AS c
        JOIN billing_address_customer AS bac ON c.client_id = bac.client_id
        JOIN billing_address AS ba ON bac.billing_id = ba.billing_id
        JOIN invoice AS i ON c.client_id = i.customer_id
        WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
        GROUP BY ba.city
        ORDER BY total_invoices DESC,total_gross  DESC;
    `)
    return await rows
}

//total gross and net billed by city
db.getSumSalesByCityProducts = async function (startDate, endDate) {
    const [rows] = await connection.query(`
    SELECT ba.city,
    COUNT(i.invoice_no) AS total_invoices,
    SUM(i.gross_total)  AS total_gross,
    SUM(i.net_total)    AS total_net,
    SUM(i.gross_total - i.net_total) AS diff
FROM customer AS c
      JOIN billing_address_customer AS bac ON c.client_id = bac.client_id
      JOIN billing_address AS ba ON bac.billing_id = ba.billing_id
      JOIN invoice AS i ON c.client_id = i.customer_id
      JOIN invoice_line il ON i.invoice_no = il.invoice_id
      JOIN product p ON il.product_code = p.product_code AND product_type = 'P'
WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
GROUP BY ba.city
ORDER BY total_invoices DESC, total_gross DESC;
    `)
    return await rows
}

//total gross and net billed by city
db.getSumSalesByCityServices = async function (startDate, endDate) {
    const [rows] = await connection.query(`
    SELECT ba.city,
    COUNT(i.invoice_no) AS total_invoices,
    SUM(i.gross_total)  AS total_gross,
    SUM(i.net_total)    AS total_net,
    SUM(i.gross_total - i.net_total) AS diff
FROM customer AS c
      JOIN billing_address_customer AS bac ON c.client_id = bac.client_id
      JOIN billing_address AS ba ON bac.billing_id = ba.billing_id
      JOIN invoice AS i ON c.client_id = i.customer_id
      JOIN invoice_line il ON i.invoice_no = il.invoice_id
      JOIN product p ON il.product_code = p.product_code AND product_type = 'S'
WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
GROUP BY ba.city
ORDER BY total_invoices DESC, total_gross DESC;
    `)
    return await rows
}

//day of the week both
db.getDayOfWeekSales = async function (startDate, endDate) {
    const [rows] = await connection.query(`
        SELECT DAYOFWEEK(i.invoice_date) AS day_of_week,
             ROUND(COUNT(*) * 100.0 / SUM(COUNT(*)) OVER (), 2) AS percentage
      FROM invoice AS i
      WHERE i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
      GROUP BY DAYOFWEEK(i.invoice_date)
      ORDER BY DAYOFWEEK(i.invoice_date)
        `)
    return await rows
}


db.getDayOfWeekSalesProducts = async function (startDate, endDate) {
    const [rows] = await connection.query(`
    SELECT DAYOFWEEK(i.invoice_date)                          AS day_of_week,
    ROUND(COUNT(*) * 100.0 / SUM(COUNT(*)) OVER (), 2) AS percentage
FROM sio.invoice AS i
      JOIN sio.invoice_line AS il ON i.invoice_no = il.invoice_id
      JOIN sio.product AS p ON il.product_code = p.product_code
WHERE p.product_type = 'P' and i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
GROUP BY DAYOFWEEK(i.invoice_date)
ORDER BY DAYOFWEEK(i.invoice_date)
        `)
    return await rows
}

db.getDayOfWeekSalesServices = async function (startDate, endDate) {
    const [rows] = await connection.query(`
    SELECT DAYOFWEEK(i.invoice_date)                          AS day_of_week,
    ROUND(COUNT(*) * 100.0 / SUM(COUNT(*)) OVER (), 2) AS percentage
FROM sio.invoice AS i
      JOIN sio.invoice_line AS il ON i.invoice_no = il.invoice_id
      JOIN sio.product AS p ON il.product_code = p.product_code
WHERE p.product_type = 'S' and i.invoice_date BETWEEN '${startDate}' AND '${endDate}'
GROUP BY DAYOFWEEK(i.invoice_date)
ORDER BY DAYOFWEEK(i.invoice_date)
        `)
    return await rows
}

//return the gross value made grouped by product name
db.getTotalSoldAndBilledProducts = async function (startDate, endDate) {
    const [rows] = await connection.query(`
        SELECT p.product_description, p.product_type, 
            COUNT(il.product_code) AS total_sold, 
            SUM(il.quantity * il.unit_price) AS total_billed 
        FROM product p
        JOIN invoice_line il ON p.product_code = il.product_code
        WHERE il.tax_point_date BETWEEN '${startDate}' AND '${endDate}'
        GROUP BY p.product_description, p.product_type
        ORDER BY total_sold DESC
        LIMIT 20;
    `)
    return await rows
}

//return the gross value made grouped by family group product
db.getTotalSoldAndBilledGroupProducts = async function (startDate, endDate) {
    const [rows] = await connection.query(`
        SELECT  p.product_group, p.product_type, 
            COUNT(il.product_code) AS total_sold, 
            SUM(il.quantity * il.unit_price) AS total_billed 
        FROM product p
        JOIN invoice_line il ON p.product_code = il.product_code
        WHERE il.tax_point_date BETWEEN '${startDate}' AND '${endDate}'
        GROUP BY p.product_group, product_type
        ORDER BY total_sold DESC
        LIMIT 10;  
    `)
    return await rows
}

//return the gross value made grouped by family group product
db.getActiveClients = async function () {
    const [rows] = await connection.query(`
        SELECT DISTINCT customer_id as num_customers
        FROM (
            SELECT customer_id, YEAR(invoice_date) as year, QUARTER(invoice_date) as quarter
            FROM invoice
            GROUP BY customer_id, YEAR(invoice_date), QUARTER(invoice_date)
            HAVING COUNT(*) >= 1
        ) as subquery
        GROUP BY customer_id
        HAVING COUNT(*) = 4
        LIMIT 20;
    `)
    return await rows
}
module.exports = db

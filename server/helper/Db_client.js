const db = require("./Db_connection.js");

db.insertMutipleCustomers = async function (data) {
  for (let i = 0; i < data.length; i++) {
    //for each customer first insert the Billing Addresses
    const placeholders = data[i].BillingAddress.map(() => "(?, ?, ?, ?)").join(", ");

    // create an array of values to insert
    const values = data[i].BillingAddress.reduce((acc, curr) => {
      acc.push(curr.AddressDetail, curr.City, curr.PostalCode, curr.Country);
      return acc;
    }, []);

    const sql = `INSERT INTO billing_address (address_detail, city, postal_code, country) VALUES ${placeholders}`;

    //first insert the billing address information
    connection
      .execute(sql, values)
      .then((results) => {
        const valuesCustomer = [data[i].CustomerID, data[i].CompanyName];
        const sqlCustomer = "INSERT INTO customer VALUES (?,?);";

        //second insert the customer information
        connection.execute(sqlCustomer, valuesCustomer).then((resultsCustomer) => {
          //then make the connection of the two tables on the billing_address_customer table
          for (let billingAddressId = results[0].insertId; billingAddressId < results[0].insertId + data[i].BillingAddress.length; billingAddressId++) {
            const valuesCustomer = [billingAddressId, data[i].CustomerID];
            const sqlCustomer = "INSERT INTO billing_address_customer VALUES (?,?);";

            connection
              .execute(sqlCustomer, valuesCustomer)
              .then(() => {})
              .catch((error) => {
                console.error(error);
              });
          }
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }
};

db.selectClients = async function () {
  const [rows] = await connection.query(
    "SELECT  c.client_id, c.client_name, SUM(i.gross_total) as total_gross, SUM(i.tax_payable) as total_taxes, SUM(i.net_total) as total_net\
      FROM sio.invoice as i\
      INNER JOIN sio.customer as c\
      ON c.client_id = i.customer_id\
      GROUP BY i.customer_id\
      ORDER BY i.customer_id ASC;"
  );
  return await rows;
};

db.invoiceCustomerInfo = async function (id) {
  const [rows] = await connection.query(
    `SELECT result.client_id, result.client_name, result.invoice_no, result.invoice_date, result.invoice_type, result.net_total, result.gross_total, result.taxes_total, SUM(il.quantity) as total_items
      FROM (
          SELECT c.client_id, c.client_name, i.invoice_no, i.invoice_date, i.invoice_type, SUM(i.net_total) as net_total, SUM(i.gross_total) as gross_total, SUM(i.tax_payable) as taxes_total
          FROM sio.customer AS c
          INNER JOIN sio.invoice AS i ON c.client_id = i.customer_id
          WHERE c.client_id = ${id}
          GROUP BY i.invoice_no, i.invoice_date, c.client_id, c.client_name, i.invoice_type
          ORDER BY i.invoice_date DESC
      ) AS result
      INNER JOIN sio.invoice_line AS il ON il.invoice_id = result.invoice_no
      GROUP BY result.invoice_no, result.client_id, result.client_name, result.invoice_date, result.invoice_type, result.net_total, result.gross_total, result.taxes_total`
  );
  return await rows;
};

db.billingAddressesCustomer = async function (id) {
  const [rows] = await connection.query(
    `SELECT c.client_id, c.client_name, ba.address_detail, ba.city, ba.postal_code, ba.country
          FROM sio.customer as c,
               sio.billing_address_customer as bac,
               sio.billing_address as ba
          WHERE bac.billing_id = ba.billing_id
            and c.client_id = bac.client_id and c.client_id = ${id};`
  );
  return await rows;
};

db.selectProductsOfClients = async function (customerId) {
  const [rows] = await connection.query(
    `SELECT DISTINCT c.client_id,
        c.client_name,
        p.product_code,
        p.product_description,
        COUNT(p.product_code) AS total_product
        FROM sio.customer AS c
            JOIN sio.invoice AS i
                ON c.client_id = i.customer_id
            JOIN sio.invoice_line AS il
                ON i.invoice_no = il.invoice_id
            JOIN sio.product AS p
                ON p.product_code = il.product_code
        WHERE c.client_id = ${customerId}
        GROUP BY c.client_id, p.product_code, p.product_description`
  );
  return await rows;
};

db.countClientsByCity = async function () {
  const [rows] = await connection.query("SELECT city, COUNT(*) as num_clients FROM billing_address GROUP BY city;");
  return await rows;
};

db.selectFamilyProductsOfClients = async function (customerId) {
  const [rows] = await connection.query(
    `SELECT DISTINCT c.client_id,
                c.client_name,
                p.product_group,
                COUNT(p.product_group) AS total_group
        FROM sio.customer AS c
         JOIN sio.invoice AS i
              ON c.client_id = i.customer_id
         JOIN sio.invoice_line AS il
              ON i.invoice_no = il.invoice_id
         JOIN sio.product AS p
              ON p.product_code = il.product_code
        WHERE c.client_id = ${customerId}
        GROUP BY c.client_id, p.product_group`
  );
  return await rows;
};

module.exports = db;

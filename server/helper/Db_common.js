const db = require("./Db_connection.js")
const mysql = require("mysql2/promise")

// delete all data in every table
db.deleteAllData = async function () {
    connection.query(
        "delete from billing_address_customer; delete from general_info; delete from invoice_line; delete from invoice; delete from billing_address; delete from customer;  delete from product; delete from tax_enum;"
    )
}

db.insertGeneralInfo = async function (data) {
    console.log("data", data)

    const sql = `INSERT INTO general_info (fiscal_year, start_date, end_date, currency_code) VALUES (${data.FiscalYear}, '${data.StartDate}', '${data.EndDate}', '${data.CurrencyCode}')`

    return connection
        .execute(sql)
        .then((results) => {
            return results
        })
        .catch((error) => {
            console.error(error)
        })
}

module.exports = db

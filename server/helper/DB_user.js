const db = require("./DB_user.js");

db.insertUser = async function (username, email, password) {
  const placeholders = "(?, ?, ?)";
  const sqlLine = `INSERT INTO sio.user (username, email, password) VALUES ${placeholders}`;
  const [result] = await connection.execute(sqlLine, [username, email, password]);
  return result.insertId;
};

db.fetchUserByUsername = async function (username) {
  const [user] = await connection.query(
    `SELECT u.user_id, u.username, u.email, u.password
        FROM sio.user as u
        WHERE u.username = '${username}';`
  );
  return user[0];
};

db.fetchUserByEmail = async function (email) {
  const [user] = await connection.query(
    `SELECT u.user_id, u.username, u.email, u.password
            FROM sio.user as u
            WHERE u.email = '${email}';`
  );
  return user[0];
};

module.exports = db;

var db = {}
const mysql = require("mysql2/promise")

db.connect = async function () {
    if (global.connection && global.connection.state !== "disconnected") {
        return global.connection
    }
    const connection = await mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "password",
        database: "sio",
        multipleStatements: true,
    })
    global.connection = connection
    console.log("Connected to the mysql database")
    return connection
}

module.exports = db;

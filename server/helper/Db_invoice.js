const db = require("./Db_connection.js");

db.selectInvoices = async function () {
  const [rows] = await connection.query("SELECT * FROM invoice;");
  return await rows;
};

//get the correspondent id (auto increment on the database) of the taxtable to reference it on the invoice line
function getTaxId(taxTable, taxOfProduct) {
  for (let i = 0; i < taxTable.length; i++) {
    if (
      taxOfProduct.TaxType === taxTable[i].TaxType &&
      taxOfProduct.TaxCountryRegion === taxTable[i].TaxCountryRegion &&
      taxOfProduct.TaxCode === taxTable[i].TaxCode &&
      taxOfProduct.TaxPercentage === taxTable[i].TaxPercentage
    ) {
      return taxTable[i].Id;
    }
  }
}

db.insertInvoices = async function (invoices, lines, taxTable) {
  for (const invoice of invoices) {
    const placeholders = "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    const values = [
      invoice.InvoiceNo,
      invoice.ATCUD,
      invoice.InvoiceStatus,
      invoice.SourceID,
      invoice.InvoiceStatusDate,
      invoice.SourceBilling,
      invoice.Hash,
      invoice.HashControl,
      invoice.Period,
      invoice.InvoiceDate,
      invoice.InvoiceType,
      invoice.CustomerID,
      invoice.TaxPayable,
      invoice.NetTotal,
      invoice.GrossTotal,
    ];

    const sql = `INSERT INTO invoice VALUES ${placeholders}`;

    const invoiceResult = await connection.execute(sql, values);

    try {
      const invoiceLines = lines.filter((line) => line.InvoiceNo === invoice.InvoiceNo);
      for (const line of invoiceLines) {
        const placeholdersLine = "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        const valuesLine = [
          line.InvoiceNo,
          String(line.LineNumber),
          line.ProductCode,
          line.Quantity,
          line.UnitOfMeasure,
          line.UnitPrice,
          line.TaxPointDate,
          line.ProductDescription,
          line.Amount,
          getTaxId(taxTable, line.Tax),
          line.Description,
        ];

        const sqlLine = `INSERT INTO invoice_line VALUES ${placeholdersLine}`;
        const lineResult = await connection.execute(sqlLine, valuesLine);
      }
    } catch (error) {
      console.log("Failed InvoiceNo: " + invoice.InvoiceNo);
      console.log(
        "Failed Invoice Lines: ",
        lines.filter((line) => line.InvoiceNo === invoice.InvoiceNo)
      );
      console.error(error);
    }
  }
};

db.invoiceLines = async function (invoiceNo) {
  const [rows] = await connection.query(
    `SELECT *
    FROM sio.invoice_line INNER JOIN sio.tax_enum
    ON sio.invoice_line.tax_id = sio.tax_enum.tax_id
    WHERE invoice_id = '${invoiceNo}';`
  );

  return await rows;
};

module.exports = db;

const db = require("./Db_connection.js")

db.selectTaxTable = async function () {
    const [rows] = await connection.query("SELECT * FROM tax_enum;")
    return await rows
}

db.insertTaxTable = async function (data) {
    //console.log(data);
    const placeholders = data.map(() => "(?, ?, ?, ?, ?)").join(", ")

    // array of values to insert
    const values = data.reduce((acc, curr) => {
        acc.push(
            curr.TaxCountryRegion,
            curr.TaxCode,
            curr.Description,
            curr.TaxPercentage,
            curr.TaxType
        )
        return acc
    }, [])

    const sql = `INSERT INTO tax_enum (tax_country_region, tax_code, description, tax_percentage, tax_type) VALUES ${placeholders}`

    return connection
        .execute(sql, values)
        .then((results) => {
            //atribute the ids of each tax of the taxtable
            for (
                let i = 0, id = results[0].insertId;
                id < results[0].insertId + data.length;
                i++, id++
            ) {
                data[i].Id = id
            }
            return data
        })
        .catch((error) => {
            console.error(error)
        })
}

module.exports = db

var db = require("../helper/Db_invoice");

var invoiceController = {};

invoiceController.list = async function (req, res) {
  const invoices = await db.selectInvoices();
  res.send({ invoices: invoices });
};

invoiceController.invoiceLines = async function (req, res, next) {
  const invoiceLines = await db.invoiceLines(req.query.invoiceNo);
  res.send({
    invoiceLines: invoiceLines,
  });
};

module.exports = invoiceController;

var parser = require("../helper/Parser")
var db = require("../helper/Db_dashboard")
var dbCommon = require("../helper/Db_common")
var apiKey = "AIzaSyCCDy6rnms9_22-cqOxpUC4Ejz38Uc8WPQ"
var fetch = require("node-fetch")

var indexController = {}

//multer configuration
const multer = require("multer")
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "files/")
    },
    filename: function (req, file, cb) {
        cb(null, `${file.originalname}`)
    },
})

const upload = multer({ storage: storage }).single("file")

const fs = require("fs")
const xml2js = require("xml2js")

indexController.parse = async function (req, res, next) {
    await db.deleteAllData()
    const xmlData = fs.readFileSync("./files/" + req.file.originalname, "utf8")
    xml2js.parseString(xmlData, function (err, result) {
        if (err) {
            console.error(err)
        } else {
            let generalInfo = parser.parseGeneralInfo(
                result.AuditFile.Header[0]
            )
            db.insertGeneralInfo(generalInfo)
            let customers = parser.parseCustomers(
                result.AuditFile.MasterFiles[0].Customer
            )
            db.insertMutipleCustomers(customers)
            let products = parser.parseProducts(
                result.AuditFile.MasterFiles[0].Product
            )
            db.insertMutipleProducts(products)
            let taxTable = parser.parseTaxTable(
                result.AuditFile.MasterFiles[0].TaxTable[0].TaxTableEntry
            )
            let invoices = parser.parseInvoices(
                result.AuditFile.SourceDocuments[0].SalesInvoices[0].Invoice
            )
            db.insertTaxTable(taxTable)
                .then((taxTableInserted) => {
                    // invoices[0] -> invoice    invoices[1] -> lines of the invoices
                    db.insertInvoices(
                        invoices[0],
                        invoices[1],
                        taxTableInserted
                    )
                })
                .catch((error) => {
                    console.error(error)
                })
        }
    })
}

indexController.uploadXML = async function (req, res, next) {
    upload(req, res, function (err) {
        // process the uploaded file
        const file = req.file
        if (err || !file) {
            return next(
                res.status(400).send({ message: "Error trying to upload file" })
            )
        }
        res.status(200).send({ message: "File uploaded successfully" })
        indexController.parse(req, res, next)
    })
}

async function getCoordinates(postalCodes) {
    let promises = postalCodes.map((postalCode) => {
        return fetch(
            `https://maps.googleapis.com/maps/api/geocode/json?address=${postalCode.postal_code}&key=${apiKey}`
        )
            .then((response) => {
                return response.json().then((json) => {
                    try {
                        return {
                            client_id: postalCode.client_id,
                            client_name: postalCode.client_name,
                            lat: json.results[0].geometry.location.lat,
                            lng: json.results[0].geometry.location.lng,
                            postalCode: postalCode.postal_code,
                            address: postalCode.address,
                            city: postalCode.city,
                            country: postalCode.country,
                        }
                    } catch (error) {
                        console.log(
                            "Error trying to get coordinates of client: " +
                                postalCode.client_name
                        )
                        return null
                    }
                })
            })
            .catch((err) => {
                console.log(
                    "Error trying to get coordinates of client: " +
                        postalCode.client_name
                )
                return null
            })
    })

    let results = await Promise.all(promises)
    return results.filter((result) => result !== null)
}

async function getCoordinatesByCity(cities) {
    let promises = cities.map((city) => {
        return fetch(
            `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(
                city.city
            )}&key=${apiKey}`
        )
            .then((response) => {
                return response.json().then((json) => {
                    try {
                        return {
                            city: city.city,
                            coordinates: [
                                json.results[0].geometry.location.lat,
                                json.results[0].geometry.location.lng,
                            ],
                            total_gross: city.total_gross,
                            total_net: city.total_net,
                            total_invoices: city.total_invoices,
                            diff: city.diff,
                        }
                    } catch (error) {
                        console.log(
                            "Error trying to get coordinates of city: " +
                                city.city
                        )
                        return null
                    }
                })
            })
            .catch((err) => {
                console.log(
                    "Error trying to get coordinates of city: " + city.city
                )
                return null
            })
    })

    let results = await Promise.all(promises)
    return results.filter((result) => result !== null)
}

indexController.stats = async function (req, res, next) {
    console.log(req.query)
    var startDate = req.query.startDate
    var endDate = req.query.endDate

    console.log(startDate)
    console.log(endDate)

    if (startDate == "" || endDate == "") {
        startDate = "1900-01-01"
        endDate = "2050-01-01"
    }

    const [
        bestClientsValue,
        bestClientsNumber,
        postalCodes,
        totals,
        countClientsAvgInvoice,
        activeClients,
        salesByMonth,
        salesByCity,
        salesByCityProducts,
        salesByCityServices,
        totalByProduct,
        totalByFamilyProduct,
        salesByDayWeek,
        salesByDayWeekProducts,
        salesByDayWeekServices,
        generalInfo,
    ] = await Promise.all([
        db.topBestClientsByValue(startDate, endDate),
        db.topBestClientsByNumberPurchases(startDate, endDate),
        db.addressOfClients(),
        db.getTotalGrossAndNet(startDate, endDate),
        db.getAvgInvoiceAndCountClients(startDate, endDate),
        db.getActiveClients(startDate, endDate),
        db.getSalesByMonth(startDate, endDate),
        db.getSumSalesByCity(startDate, endDate),
        db.getSumSalesByCityProducts(startDate, endDate),
        db.getSumSalesByCityServices(startDate, endDate),
        db.getTotalSoldAndBilledProducts(startDate, endDate),
        db.getTotalSoldAndBilledGroupProducts(startDate, endDate),
        db.getDayOfWeekSales(startDate, endDate),
        db.getDayOfWeekSalesProducts(startDate, endDate),
        db.getDayOfWeekSalesServices(startDate, endDate),
        db.getGeneralInfo(),
    ])

    const coordinatesClients = await getCoordinates(postalCodes)
    const coordinatesCitiesBySales = await getCoordinatesByCity(salesByCity)
    const coordinatesCitiesBySalesProducts = await getCoordinatesByCity(
        salesByCityProducts
    )
    const coordinatesCitiesBySalesServices = await getCoordinatesByCity(
        salesByCityServices
    )

    const generalStats = {
        total_net: Number(totals[0].total_net).toFixed(2),
        total_gross: Number(totals[0].total_gross).toFixed(2),
        num_clients: countClientsAvgInvoice[0].num_clients,
        avg_invoice: Number(countClientsAvgInvoice[0].avg_invoice).toFixed(2),
        active_clients: activeClients.length,
    }

    res.send({
        bestClientsValue,
        bestClientsNumber,
        coordinatesClients,
        generalStats,
        salesByMonth,
        coordinatesCitiesBySales,
        coordinatesCitiesBySalesProducts,
        coordinatesCitiesBySalesServices,
        totalByProduct,
        totalByFamilyProduct,
        salesByDayWeek,
        salesByDayWeekProducts,
        salesByDayWeekServices,
        generalInfo,
    })
}

indexController.delete = async function (req, res, next) {
    await dbCommon.deleteAllData()

    res.send()
}

module.exports = indexController

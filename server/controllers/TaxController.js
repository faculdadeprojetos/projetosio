var db = require("../helper/Db_tax");

var taxesController = {};

taxesController.list = async function (req, res) {
  const taxes = await db.selectTaxTable();
  res.send({ taxes: taxes });
};

module.exports = taxesController;

var db = require("../helper/DB_user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

require("dotenv").config();

var authController = {};

authController.register = async function (req, res) {
  var { username, email, password, confirmPassword } = req.body;

  try {
    let user = await db.fetchUserByUsername(username);

    if (user) {
      console.log("a")
      return res.status(400).json({ message: "Username already in use" });
    }

    user = await db.fetchUserByEmail(email);

    if (user) {
      console.log("b")
      return res.status(400).json({ message: "Email already in use" });
    }

    if (password != confirmPassword) {
      console.log("c")
      return res.status(400).json({ message: "Passwords do not match" });
    }

    const salt = await bcrypt.genSalt(10);
    password = await bcrypt.hash(password, salt);

    const id = await db.insertUser(username, email, password);

    const payload = {
      user: {
        id: id,
      },
    };

    jwt.sign(payload, process.env.SECRET_KEY, { expiresIn: "3h" }, (err, token) => {
      if (err) {
        throw err;
      }
      res.json({ token });
    });
  } catch (err) {
    console.log(err)
    res.status(500).send("Server Error");
  }
};

authController.login = async function (req, res) {
  const { username, password } = req.body;

  try {
    let user = await db.fetchUserByUsername(username);
    if (!user) {
      return res.status(400).json({ message: "Invalid credentials" });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({ message: "Invalid credentials" });
    }

    const payload = {
      user: {
        id: user.user_id,
      },
    };

    jwt.sign(payload, process.env.SECRET_KEY, { expiresIn: "1h" }, (err, token) => {
      if (err) {
        throw err;
      }
      res.json({ token });
    });
  } catch (err) {
    res.status(500).send("Server Error");
  }
};

module.exports = authController;

var db = require("../helper/Db_client")
var dbClient = require("../helper/Db_client")

var clientController = {}

clientController.list = async function (req, res) {
    const clients = await db.selectClients()
    res.send({ clients: clients })
}

clientController.detailedInfo = async function (customerId, req, res, next) {
    const invoiceInfo = await db.invoiceCustomerInfo(customerId)
    const addressInfo = await db.billingAddressesCustomer(customerId)
    const productsInfo = await dbClient.selectProductsOfClients(customerId)
    const productFamiliesInfo = await dbClient.selectFamilyProductsOfClients(
        customerId
    )
    res.send({
        addressInfo: addressInfo,
        invoiceInfo: invoiceInfo,
        productsInfo: productsInfo,
        productFamiliesInfo: productFamiliesInfo,
    })
}

module.exports = clientController

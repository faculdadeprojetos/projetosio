var db = require("../helper/Db_products");

var productController = {};

productController.list = async function (req, res) {
  const products = await db.selectProducts();
  res.send({ products: products });
};

productController.insert = async function (req, res) {
  console.log(req.body);
  const insertConfirmation = await db.insertProduct(req.body);
  console.log(insertConfirmation);
  res.send({ sucess: insertConfirmation });
};

productController.detailedInfo = async function (productId, req, res, next) {
  const total_in_sales = await db.totalInSales(productId);
  const total_sales = await db.totalSales(productId);
  const best_local = await db.bestLocal(productId);
  const product_sales = await db.productSales(productId);
  res.send({
    total_in_sales: total_in_sales,
    total_sales: total_sales,
    best_local: best_local,
    product_sales: product_sales
  })
}

module.exports = productController;

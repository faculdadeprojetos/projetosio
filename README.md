# Simulation of a business process

This project developed within the scope of the SIO curricular unit.

## About the project
This project has the purpose of simulate a business project by using Node.js, React JS and MySql as database. A SAF-T PT(.XML file) should be exported from an ERP and then imported to this project to have all the functionalities available. 

## Basic Overview 
- Import a SAF-T PT file.
- ..
- ..
- ..
- ..

## Instructions

- Execute the command to clone the project &#8595;

```
git clone https://gitlab.estg.ipp.pt/sio2223_07/sio2223_07
```

**Server side**

- Execute the following commands to get into the server folder, install all the dependencies and start the Node.js project &#8595;

```
cd server
npm install
nodemon
```

**Client side**

- Execute the following commands to get into the client folder, install all the dependencies and start the React.js project &#8595;

```
cd client
npm install
npm start
```

**Docker container**

- If you don't have docker installed, follow this [link](https://www.docker.com) and install the latest version.
- Run the following command to get the container with mysql &#8595;

```
docker run -p 3306:3306 --name sio-mysql-db -e MYSQL_ROOT_PASSWORD=password -d mysql:8.0.32
```
- If everything works, you will get the id of the container that was just created, then run the next command &#8595;

```
docker exec -it sio-mysql-db /bin/bash
```

- Enter on the mysql, by running this command (the password is "password"). &#8595;

```
mysql -u root -p
```

- After login in copy the .sql file available on this repository, on the server folder, to create the database and all his content.

## Authors
- Josué Freitas (8200308@estg.ipp.pt)
- Nuno Ribeiro (8200309@estg.ipp.pt)
- Regina Neto (8200279@estg.ipp.pt)
- Rui Neto (8200321@estg.ipp.pt)
- Simão Santos (8200322@estg.ipp.pt)


## License

[![License: Unlicense](https://img.shields.io/badge/license-Unlicense-blue.svg)](http://unlicense.org/)
